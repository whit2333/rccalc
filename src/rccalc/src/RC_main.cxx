#include "RC_main.h"
#include "InSANEMathFunc.h"

#include "InSANEPOLRADBornDiffXSec.h"
#include "InSANEPOLRADInelasticTailDiffXSec.h"
#include "InSANEPOLRADQuasiElasticTailDiffXSec.h"
#include "InSANEPOLRADElasticTailDiffXSec.h"

//______________________________________________________________________________
MyMainFrame::MyMainFrame(const TGWindow *p,UInt_t w,UInt_t h) {

   fXSec_id      = 0;
   fWidth        = w;
   fHeight       = h;
   fSideBarWidth = 250;
   fNPoints      = 50;
   fBeamEnergy   = 4.7;
   fColor        = 4;
   fTargetNucleus   = InSANENucleus::Proton();
   fDiffXSec        = 0;
   fHelicity     = 0;
   fIsInternalOnly = false;
   fIsExternalOnly = false;

   // main frame - vertical stacking layout
   fMainFrame = new TGMainFrame(p,fWidth,fHeight,kMainFrame | kVerticalFrame);
   fMainFrame->SetName("fMainFrame");
   fMainFrame->Connect("Destroyed()", "MyMainFrame", this, "Destroy()");

   // horizontal frame
   fHorizontalFrame1 = new TGHorizontalFrame(fMainFrame,fWidth,fHeight,kHorizontalFrame);
   fHorizontalFrame1->SetName("fHorizontalFrame1");

   // vertical frame
   fVertLeftFrame = new TGVerticalFrame(fHorizontalFrame1,fSideBarWidth,fHeight,kVerticalFrame);
   fVertLeftFrame->SetName("fVertLeftFrame");

   // output files text entry
   TGFont *ufont = gClient->GetFont("-*-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1");
   TGGC   *uGC;           // will reflect user GC changes

   // graphics context changes
   GCValues_t valEntry1353; 
   valEntry1353.fMask = kGCForeground | kGCBackground | kGCFillStyle | kGCFont | kGCGraphicsExposures;
   gClient->GetColorByName("#000000",valEntry1353.fForeground);
   gClient->GetColorByName("#e0e0e0",valEntry1353.fBackground);
   valEntry1353.fFillStyle         = kFillSolid;
   valEntry1353.fFont              = ufont->GetFontHandle();
   valEntry1353.fGraphicsExposures = kFALSE;
   uGC = gClient->GetGC(&valEntry1353, kTRUE);

   TGLayoutHints * lh_0 = new TGLayoutHints(kLHintsLeft | kLHintsCenterY ,0,0,0,0);
   TGLayoutHints * lh_1 = new TGLayoutHints(kLHintsLeft | kLHintsCenterY ,1,1,1,1);

   // file name 
   TGTextEntry *fTextEntry1353 = new TGTextEntry(fVertLeftFrame, new TGTextBuffer(31),-1,uGC->GetGC(),ufont->GetFontStruct(),kSunkenFrame | kDoubleBorder | kOwnBackground);
   fTextEntry1353->SetMaxLength(300);
   fTextEntry1353->SetAlignment(kTextLeft);
   fTextEntry1353->SetText("RCcalc_test");
   fTextEntry1353->Resize(fSideBarWidth-20,fTextEntry1353->GetDefaultHeight());
   fTextEntry1353->SetToolTipText("Output file name with no extentions");
   fVertLeftFrame->AddFrame(fTextEntry1353, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));

   // -----------------
   //TGNumberEntry *fNumberEntry1974 = new TGNumberEntry(fVertLeftFrame, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
   //fNumberEntry1974->SetName("fNumberEntry1974");
   //fVertLeftFrame->AddFrame(fNumberEntry1974, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));

   // -----------------
   // npoints and color
   TGHorizontalFrame  * hf_npoints    = new TGHorizontalFrame(fVertLeftFrame,fSideBarWidth,22,kHorizontalFrame);
   TGLabel            * label_npoints = new TGLabel(hf_npoints,"N points");
   label_npoints->SetTextJustify(36);
   label_npoints->SetMargins(0,0,0,0);
   label_npoints->SetWrapLength(-1);
   label_npoints->SetMinWidth(50);
   label_npoints->SetMaxWidth(51);
   hf_npoints->AddFrame(label_npoints,new TGLayoutHints(kLHintsLeft,1,1,1,1));

   fNumberEntry_npoints = new TGNumberEntry(hf_npoints, (Double_t) 0,4,TGNumberFormat::kNESInteger,(TGNumberFormat::EStyle) 5);
   fNumberEntry_npoints->SetName("npoints");
   fNumberEntry_npoints->SetLimits(TGNumberFormat::kNELLimitMinMax,0.0001,1000.0);
   fNumberEntry_npoints->SetNumber(50);
   hf_npoints->AddFrame(fNumberEntry_npoints, new TGLayoutHints(kLHintsLeft ,2,2,2,2));

   fNumberEntry_color = new TGNumberEntry(hf_npoints, (Double_t) 0,4,TGNumberFormat::kNESInteger,(TGNumberFormat::EStyle) 5);
   fNumberEntry_color->SetName("color");
   fNumberEntry_color->SetLimits(TGNumberFormat::kNELLimitMinMax,1,2000);
   fNumberEntry_color->SetNumber(4);
   fNumberEntry_color->Connect("ValueSet(Long_t)", "MyMainFrame", this, "DoSlider()");
   hf_npoints->AddFrame(fNumberEntry_color, new TGLayoutHints(kLHintsLeft ,2,2,2,2));

   fNumberEntry_style = new TGNumberEntry(hf_npoints, (Double_t) 0,4,TGNumberFormat::kNESInteger,(TGNumberFormat::EStyle) 5);
   fNumberEntry_style->SetName("style");
   fNumberEntry_style->SetLimits(TGNumberFormat::kNELLimitMinMax,1,1000);
   fNumberEntry_style->SetNumber(1);
   fNumberEntry_style->Connect("ValueSet(Long_t)", "MyMainFrame", this, "DoSlider()");
   hf_npoints->AddFrame(fNumberEntry_style, new TGLayoutHints(kLHintsLeft ,2,2,2,2));

   fVertLeftFrame->AddFrame(hf_npoints, new TGLayoutHints(kLHintsLeft | kLHintsExpandX,2,2,2,2));

   // ------------------------
   // Target settings
   TGGroupFrame * groupFrame = new TGGroupFrame(fVertLeftFrame,"Target",kVerticalFrame);
   groupFrame->SetTitlePos(TGGroupFrame::kLeft);

   fListBox_targets = new TGComboBox(groupFrame, 90);
   fListBox_targets->AddEntry( "proton   " ,0);
   fListBox_targets->AddEntry( "neutron  " ,1);
   fListBox_targets->AddEntry( "deuteron " ,2);
   fListBox_targets->AddEntry( "3He      " ,3);
   fListBox_targets->AddEntry( "4He      " ,4);
   fListBox_targets->AddEntry( "12C      " ,5);
   fListBox_targets->AddEntry( "56Fe     " ,6);
   fListBox_targets->Select(0);
   fListBox_targets->Resize(150, 22);
   groupFrame->AddFrame(fListBox_targets, new TGLayoutHints(kLHintsLeft ,2,2,2,2));
   fListBox_targets->Connect("Selected(Int_t)","MyMainFrame",this,"SetTarget(Int_t)");

   // Z and A
   TGHorizontalFrame  * hf_ZA    = new TGHorizontalFrame(groupFrame,fSideBarWidth,22,kHorizontalFrame);
   TGLabel            * label_Z = new TGLabel(hf_ZA,"Z:");
   label_Z->SetTextJustify(36);
   label_Z->SetMargins(0,0,0,0);
   label_Z->SetWrapLength(-1);
   label_Z->SetMinWidth(50);
   label_Z->SetMaxWidth(51);
   hf_ZA->AddFrame(label_Z,new TGLayoutHints(kLHintsLeft | kLHintsCenterY ,1,1,1,1));

   fNumberEntry_Z = new TGNumberEntry(hf_ZA, (Double_t) 0,6,TGNumberFormat::kNESInteger,(TGNumberFormat::EStyle) 5);
   fNumberEntry_Z->SetName("Z");
   fNumberEntry_Z->SetLimits(TGNumberFormat::kNELLimitMinMax,0,110);
   fNumberEntry_Z->SetNumber(1);
   fNumberEntry_Z->SetWidth(40);
   hf_ZA->AddFrame(fNumberEntry_Z, new TGLayoutHints(kLHintsLeft | kLHintsCenterY ,1,1,1,1));

   TGLabel            * label_A = new TGLabel(hf_ZA,"A:");
   label_A->SetTextJustify(36);
   label_A->SetMargins(0,0,0,0);
   label_A->SetWrapLength(-1);
   label_A->SetMinWidth(50);
   label_A->SetMaxWidth(51);
   hf_ZA->AddFrame(label_A,new TGLayoutHints(kLHintsLeft | kLHintsCenterY,1,1,1,1));

   fNumberEntry_A = new TGNumberEntry(hf_ZA, (Double_t) 0,6,TGNumberFormat::kNESInteger,(TGNumberFormat::EStyle) 5);
   fNumberEntry_A->SetName("A");
   fNumberEntry_A->SetLimits(TGNumberFormat::kNELLimitMinMax,1,300);
   fNumberEntry_A->SetNumber(1);
   fNumberEntry_A->SetWidth(40);
   hf_ZA->AddFrame(fNumberEntry_A, new TGLayoutHints(kLHintsLeft | kLHintsCenterY,1,1,1,1));

   groupFrame->AddFrame(hf_ZA, new TGLayoutHints(kLHintsLeft | kLHintsExpandX,2,2,2,2));

   // Radiation Lengths
   TGHorizontalFrame  * hf_radlen    = new TGHorizontalFrame(groupFrame,fSideBarWidth,22,kHorizontalFrame);
   TGLabel            * label_Tb = new TGLabel(hf_radlen,"T_i");
   label_Tb->SetTextJustify(36);
   label_Tb->SetMargins(0,0,0,0);
   label_Tb->SetWrapLength(-1);
   hf_radlen->AddFrame(label_Tb,new TGLayoutHints(kLHintsLeft | kLHintsCenterY ,1,1,1,1));

   fNumberEntry_Tb = new TGNumberEntry(hf_radlen, (Double_t) 0,6,TGNumberFormat::kNESRealThree,(TGNumberFormat::EStyle) 5);
   fNumberEntry_Tb->SetName("Tb");
   fNumberEntry_Tb->SetLimits(TGNumberFormat::kNELLimitMinMax,0.0,100.0);
   fNumberEntry_Tb->SetNumber(0.050);
   fNumberEntry_Tb->SetWidth(50);
   hf_radlen->AddFrame(fNumberEntry_Tb, new TGLayoutHints(kLHintsLeft | kLHintsCenterY ,1,1,1,1));

   TGLabel            * label_Ta = new TGLabel(hf_radlen,"T_f");
   label_Ta->SetTextJustify(36);
   label_Ta->SetMargins(0,0,0,0);
   label_Ta->SetWrapLength(-1);
   label_Ta->SetMinWidth(50);
   label_Ta->SetMaxWidth(51);
   hf_radlen->AddFrame(label_Ta,lh_1);

   fNumberEntry_Ta = new TGNumberEntry(hf_radlen, (Double_t) 0,6,TGNumberFormat::kNESRealThree,(TGNumberFormat::EStyle) 5);
   fNumberEntry_Ta->SetName("Ta");
   fNumberEntry_Ta->SetLimits(TGNumberFormat::kNELLimitMinMax,0.0,100.0);
   fNumberEntry_Ta->SetNumber(0.050);
   fNumberEntry_Ta->SetWidth(50);
   hf_radlen->AddFrame(fNumberEntry_Ta,lh_1);

   groupFrame->AddFrame(hf_radlen, new TGLayoutHints(kLHintsLeft | kLHintsExpandX,2,2,2,2));

   // equiv. rad. thickness
   TGHorizontalFrame  * hf_tr    = new TGHorizontalFrame(groupFrame,fSideBarWidth,22,kHorizontalFrame);

   TGLabel *fLabel_tr = new TGLabel(hf_tr,"t_r=");
   fLabel_tr->SetTextJustify(36);
   fLabel_tr->SetMargins(0,0,0,0);
   fLabel_tr->SetWrapLength(-1);
   hf_tr->AddFrame(fLabel_tr,lh_1);

   fTE_tr = new TGTextEntry(hf_tr, fTB_tr = new TGTextBuffer(5), TEId_tr);
   fTE_tr->SetWidth(50);
   fTB_tr->AddText(0,"0.0");
   hf_tr->AddFrame(fTE_tr,lh_1);

   groupFrame->AddFrame(hf_tr,lh_1);

   fVertLeftFrame->AddFrame(groupFrame,new TGLayoutHints(kLHintsTop|kLHintsExpandX, 2, 1, 0, 0));

   // -------------------------------------------------------------------
   // Cross Section 
   TGGroupFrame * gf_xsec = new TGGroupFrame(fVertLeftFrame,"Cross Section",kVerticalFrame);
   gf_xsec->SetTitlePos(TGGroupFrame::kLeft);

   // list box widget containing 10 entries
   fListBox_xsec = new TGComboBox(gf_xsec, 90);
   fListBox_xsec->AddEntry( "F1F209 DIS               " ,0);
   fListBox_xsec->AddEntry( "F1F209 QuasiElastic Peak " ,1);
   fListBox_xsec->AddEntry( "POLRAD DIS               " ,2);
   fListBox_xsec->AddEntry( "POLRAD QuasiElastic Peak " ,3);
   fListBox_xsec->AddEntry( "POLRAD Elastic           " ,4);
   fListBox_xsec->AddEntry( "PDF model DIS            " ,5);
   fListBox_xsec->Select(0);
   fListBox_xsec->Resize(fSideBarWidth-20, 22);
   fListBox_xsec->Connect("Selected(Int_t)","MyMainFrame",this,"SetXSec(Int_t)");
   gf_xsec->AddFrame(fListBox_xsec,new TGLayoutHints(kLHintsTop|kLHintsLeft, 2, 2, 2, 2));

   // Options 
   TGHorizontalFrame  * hf_xsec    = new TGHorizontalFrame(gf_xsec,fSideBarWidth,22,kHorizontalFrame);
   TGCompositeFrame * vf_xsec = 0;

   // General xsec options
   vf_xsec = new TGVerticalFrame(hf_xsec, (1.0/3.0)*fSideBarWidth, 10, kFixedWidth );

   fCheckButton_born = new TGCheckButton(vf_xsec,"Born XS");
   fCheckButton_born->SetTextJustify(36);
   fCheckButton_born->SetMargins(0,0,0,0);
   fCheckButton_born->SetWrapLength(-1);
   vf_xsec->AddFrame(fCheckButton_born, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   fCheckButton_born->Connect("Toggled(Bool_t)","MyMainFrame",this,"SetBorn(Bool_t)");

   fRadioButtons_helicity = new TGButtonGroup(vf_xsec,"Hel",kVerticalFrame);
   fR_hel[0] = new TGRadioButton(fRadioButtons_helicity,new TGHotString(" 1"),BGId_HelPlus);
   fR_hel[1] = new TGRadioButton(fRadioButtons_helicity,new TGHotString(" 0"),BGId_HelZero);
   fR_hel[2] = new TGRadioButton(fRadioButtons_helicity,new TGHotString("-1"),BGId_HelMinus);
   fR_hel[1]->SetState(kButtonDown);
   fRadioButtons_helicity->Show();
   fRadioButtons_helicity->Connect("Released(Int_t)","MyMainFrame",this,"SetXSecHelicity(Int_t)");
   vf_xsec->AddFrame(fRadioButtons_helicity, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));

   hf_xsec->AddFrame(vf_xsec,new TGLayoutHints(kLHintsTop|kLHintsLeft, 1, 1, 0, 0));

   // Frame for internal/external options
   TGVerticalFrame * vf_xsec_opt = new TGVerticalFrame(hf_xsec, (2.0/3.0)*fSideBarWidth, 10, kFixedWidth );

   // Internal RC options
   vf_xsec = new TGGroupFrame(vf_xsec_opt,"Internal",kVerticalFrame | kFixedWidth);
   vf_xsec->SetWidth((2.0/3.0)*fSideBarWidth);
   //vf_xsec = new TGVerticalFrame(hf_xsec, (2.0/3.0)*fSideBarWidth, 10, kFixedWidth );

   fCheckButton_internal = new TGCheckButton(vf_xsec,"Internal Only");
   fCheckButton_internal->SetTextJustify(36);
   fCheckButton_internal->SetMargins(0,0,0,0);
   fCheckButton_internal->SetWrapLength(-1);
   vf_xsec->AddFrame(fCheckButton_internal, new TGLayoutHints(kLHintsLeft | kLHintsTop,0,0,2,2));
   fCheckButton_internal->Connect("Toggled(Bool_t)","MyMainFrame",this,"SetInternalOnly(Bool_t)");

   fCheckButton_equivrad = new TGCheckButton(vf_xsec,"Equivalent radiators");
   fCheckButton_equivrad->SetTextJustify(36);
   fCheckButton_equivrad->SetMargins(0,0,0,0);
   fCheckButton_equivrad->SetWrapLength(-1);
   vf_xsec->AddFrame(fCheckButton_equivrad, new TGLayoutHints(kLHintsLeft | kLHintsTop,0,0,2,2));
   fCheckButton_equivrad->Connect("Toggled(Bool_t)","MyMainFrame",this,"SetEquivRad(Bool_t)");
   fCheckButton_equivrad->Toggle(true);

   fCheckButton_anglepeaking = new TGCheckButton(vf_xsec,"Angle Peaking Approx");
   fCheckButton_anglepeaking->SetTextJustify(36);
   fCheckButton_anglepeaking->SetMargins(0,0,0,0);
   fCheckButton_anglepeaking->SetWrapLength(-1);
   vf_xsec->AddFrame(fCheckButton_anglepeaking, new TGLayoutHints(kLHintsLeft | kLHintsTop,0,0,2,2));
   fCheckButton_anglepeaking->Connect("Toggled(Bool_t)","MyMainFrame",this,"SetAnglePeaking(Bool_t)");

   vf_xsec_opt->AddFrame(vf_xsec,new TGLayoutHints(kLHintsTop|kLHintsLeft, 0, 0, 0, 0));

   // External RC options
   vf_xsec = new TGGroupFrame(vf_xsec_opt,"External",kVerticalFrame);
   vf_xsec->SetWidth((2.0/3.0)*fSideBarWidth);

   fCheckButton_external = new TGCheckButton(vf_xsec,"External Only");
   fCheckButton_external->SetTextJustify(36);
   fCheckButton_external->SetMargins(0,0,0,0);
   fCheckButton_external->SetWrapLength(-1);
   vf_xsec->AddFrame(fCheckButton_external, new TGLayoutHints(kLHintsLeft | kLHintsTop,0,0,2,2));
   fCheckButton_external->Connect("Toggled(Bool_t)","MyMainFrame",this,"SetExternalOnly(Bool_t)");

   vf_xsec_opt->AddFrame(vf_xsec,new TGLayoutHints(kLHintsTop|kLHintsLeft, 0, 0, 0, 0));

   hf_xsec->AddFrame(vf_xsec_opt,new TGLayoutHints(kLHintsTop|kLHintsLeft, 0, 0, 0, 0));

   gf_xsec->AddFrame(hf_xsec,new TGLayoutHints(kLHintsTop|kLHintsLeft, 0, 0, 0, 0));

   fVertLeftFrame->AddFrame(gf_xsec,new TGLayoutHints(kLHintsTop|kLHintsLeft, 0, 0, 0, 0));

   // -------------------------------------------------------------------
   // Kinematics 
   TGGroupFrame * gf_kine = new TGGroupFrame(fVertLeftFrame,"Kinematics",kVerticalFrame);

   TGHorizontalFrame *gf_hf_kine = new TGHorizontalFrame(gf_kine, 0,0,0 );

   TGVerticalFrame *label_frame_kine = new TGVerticalFrame(gf_hf_kine, 60, 10, kFixedWidth );
   TGVerticalFrame *entry_frame_kine = new TGVerticalFrame(gf_hf_kine, 10, 10);
   TGVerticalFrame *unit_frame_kine = new TGVerticalFrame(gf_hf_kine, 10, 10);
   TGCompositeFrame   * ft = 0;
   TGLayoutHints      * lh = new TGLayoutHints(kLHintsLeft | kLHintsCenterY,0,0,0,0);
   int fixedHeight = 22;

   // ------------
   // beam energy
   ft = new TGCompositeFrame(label_frame_kine, 10, fixedHeight, kFixedHeight);
   TGLabel            * label_ebeam = new TGLabel(ft,"E_beam");
   label_ebeam->SetTextJustify(36);
   label_ebeam->SetMargins(0,0,0,0);
   label_ebeam->SetWrapLength(-1);
   label_ebeam->SetMinWidth(50);
   label_ebeam->SetMaxWidth(51);
   ft->AddFrame(label_ebeam, lh);
   label_frame_kine->AddFrame(ft,lh);

   ft = new TGCompositeFrame(entry_frame_kine, 10, fixedHeight, kFixedHeight);
   fNumberEntry_ebeam = new TGNumberEntry(ft, 4.7, 5,TGNumberFormat::kNESRealThree,(TGNumberFormat::EStyle) 5);
   fNumberEntry_ebeam->SetName("ebeam");
   fNumberEntry_ebeam->SetLimits(TGNumberFormat::kNELLimitMinMax,0.0001,1000.0);
   fNumberEntry_ebeam->SetNumber(4.7);
   fNumberEntry_ebeam->Connect("ValueSet(Long_t)", "MyMainFrame", this, "DoSlider()");
   ft->AddFrame(fNumberEntry_ebeam, lh);
   entry_frame_kine->AddFrame(ft, lh);

   ft = new TGCompositeFrame(unit_frame_kine, 10, fixedHeight, kFixedHeight);
   TGLabel            * unit_ebeam = new TGLabel(ft,"GeV");
   unit_ebeam->SetTextJustify(36);
   unit_ebeam->SetMargins(0,0,0,0);
   unit_ebeam->SetWrapLength(-1);
   ft->AddFrame(unit_ebeam,lh);
   unit_frame_kine->AddFrame(ft,lh);

   // Q2 display
   ft = new TGCompositeFrame(label_frame_kine, 10, fixedHeight, kFixedHeight);
   TGLabel *fLabel_Q2 = new TGLabel(ft,"Q^2=");
   fLabel_Q2->SetTextJustify(36);
   fLabel_Q2->SetMargins(0,0,1,1);
   fLabel_Q2->SetWrapLength(-1);
   ft->AddFrame(fLabel_Q2,lh);
   label_frame_kine->AddFrame(ft, lh);

   ft = new TGCompositeFrame(entry_frame_kine, 10, fixedHeight, kFixedHeight);
   fTE_Q2 = new TGTextEntry(ft, fTB_Q2 = new TGTextBuffer(5), TEId_Q2);
   fTE_Q2->SetWidth(50);
   fTB_Q2->AddText(0, "0.0");
   ft->AddFrame(fTE_Q2,lh);
   entry_frame_kine->AddFrame(ft, lh);

   ft = new TGCompositeFrame(unit_frame_kine, 10, fixedHeight, kFixedHeight);
   TGLabel            * unit_Q2 = new TGLabel(ft,"(GeV/c)^2");
   unit_Q2->SetTextJustify(36);
   unit_Q2->SetMargins(0,0,0,0);
   unit_Q2->SetWrapLength(-1);
   ft->AddFrame(unit_Q2,lh);
   unit_frame_kine->AddFrame(ft,lh);

   gf_hf_kine->AddFrame(label_frame_kine , new TGLayoutHints(kLHintsLeft ,0,0,0,0));
   gf_hf_kine->AddFrame(entry_frame_kine , new TGLayoutHints(kLHintsLeft ,0,0,0,0));
   gf_hf_kine->AddFrame(unit_frame_kine , new TGLayoutHints(kLHintsLeft  ,0,0,0,0));

   gf_kine->AddFrame(gf_hf_kine , new TGLayoutHints(kLHintsLeft  ,0,0,0,0));

   //-----------
   // eprime slider
   TGLabel * eprime_label = new TGLabel(gf_kine,"E' (GeV):");
   gf_kine->AddFrame(eprime_label,new TGLayoutHints(kLHintsLeft,0,0,0,0));

   fSlider_eprime = new TGTripleHSlider(gf_kine,100,kDoubleScaleBoth,HSId_eprime, kHorizontalFrame);
   fSlider_eprime->SetConstrained(kTRUE);
   fSlider_eprime->SetRange(0.0001, 10.0);
   fSlider_eprime->SetPosition(1.0, 2.1);
   fSlider_eprime->SetPointerPosition(1.2);
   fSlider_eprime->Connect("PointerPositionChanged()", "MyMainFrame", this, "DoSlider()");
   fSlider_eprime->Connect("PositionChanged()", "MyMainFrame", this, "DoSlider()");
   gf_kine->AddFrame(fSlider_eprime,new TGLayoutHints(kLHintsExpandX,4,4,2,5));

   TGHorizontalFrame * fHframe3 = new TGHorizontalFrame(gf_kine, fSideBarWidth, 30 );
   fTeh1_eprime = new TGTextEntry(fHframe3, fTbh1_eprime = new TGTextBuffer(5), TEId1_eprime);
   fTeh2_eprime = new TGTextEntry(fHframe3, fTbh2_eprime = new TGTextBuffer(5), TEId2_eprime);
   fTeh3_eprime = new TGTextEntry(fHframe3, fTbh3_eprime = new TGTextBuffer(5), TEId3_eprime);
   fTeh1_eprime->SetWidth(50);
   fTeh2_eprime->SetWidth(50);
   fTeh3_eprime->SetWidth(50);
   fTeh1_eprime->SetToolTipText("Minimum (left) Value of Slider");
   fTeh2_eprime->SetToolTipText("Pointer Position Value");
   fTeh3_eprime->SetToolTipText("Maximum (right) Value of Slider");
   fTbh1_eprime->AddText(0, "0.0");
   fTbh2_eprime->AddText(0, "0.0");
   fTbh3_eprime->AddText(0, "0.0");

   fBfly1 = new TGLayoutHints(kLHintsTop | kLHintsCenterX, 0, 0, 0, 0);
   fBfly2 = new TGLayoutHints(kLHintsTop | kLHintsLeft,    0, 0, 0, 0);
   fBfly3 = new TGLayoutHints(kLHintsTop | kLHintsRight,   0, 0, 0, 0);
   fTeh1_eprime->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fTeh2_eprime->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fTeh3_eprime->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fHframe3->AddFrame(fTeh1_eprime, fBfly2);
   fHframe3->AddFrame(fTeh2_eprime, fBfly1);
   fHframe3->AddFrame(fTeh3_eprime, fBfly3);
   fHframe3->Resize(fSideBarWidth, 25);
   gf_kine->AddFrame(fHframe3,new TGLayoutHints(kLHintsExpandX,0,0,0,0));

   // theta slider
   TGLabel * theta_label = new TGLabel(gf_kine,"Theta (deg):");
   gf_kine->AddFrame(theta_label,new TGLayoutHints(kLHintsLeft,0,0,0,0));

   fSlider_theta = new TGTripleHSlider(gf_kine,100,kDoubleScaleBoth,HSId1, kHorizontalFrame);
   fSlider_theta->SetConstrained(kTRUE);
   fSlider_theta->SetRange(0.1, 179.9);
   fSlider_theta->SetPosition(20, 60);
   fSlider_theta->SetPointerPosition(40);
   fSlider_theta->Connect("PointerPositionChanged()", "MyMainFrame", this, "DoSlider()");
   fSlider_theta->Connect("PositionChanged()", "MyMainFrame", this, "DoSlider()");
   gf_kine->AddFrame(fSlider_theta,new TGLayoutHints(kLHintsExpandX,4,4,2,5));

   TGHorizontalFrame * fHframe2 = new TGHorizontalFrame(gf_kine, fSideBarWidth, 30 );
   fTeh1_theta = new TGTextEntry(fHframe2, fTbh1_theta = new TGTextBuffer(5), TEId1_theta);
   fTeh2_theta = new TGTextEntry(fHframe2, fTbh2_theta = new TGTextBuffer(5), TEId2_theta);
   fTeh3_theta = new TGTextEntry(fHframe2, fTbh3_theta = new TGTextBuffer(5), TEId3_theta);
   fTeh1_theta->SetWidth(50);
   fTeh2_theta->SetWidth(50);
   fTeh3_theta->SetWidth(50);
   fTeh1_theta->SetToolTipText("Minimum (left) Value of Slider");
   fTeh2_theta->SetToolTipText("Pointer Position Value");
   fTeh3_theta->SetToolTipText("Maximum (right) Value of Slider");
   fTbh1_theta->AddText(0, "0.0");
   fTbh2_theta->AddText(0, "0.0");
   fTbh3_theta->AddText(0, "0.0");

   fTeh1_theta->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fTeh2_theta->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fTeh3_theta->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fHframe2->AddFrame(fTeh1_theta, fBfly2);
   fHframe2->AddFrame(fTeh2_theta, fBfly1);
   fHframe2->AddFrame(fTeh3_theta, fBfly3);
   fHframe2->Resize(fSideBarWidth, 25);
   gf_kine->AddFrame(fHframe2,new TGLayoutHints(kLHintsExpandX,0,0,0,0));

   //-----------
   // W slider
   TGLabel * W_label = new TGLabel(gf_kine,"W (GeV):");
   gf_kine->AddFrame(W_label,new TGLayoutHints(kLHintsLeft,0,0,0,0));

   fSlider_W = new TGTripleHSlider(gf_kine,100,kDoubleScaleBoth,HSId_W, kHorizontalFrame);
   fSlider_W->SetConstrained(kTRUE);
   fSlider_W->SetRange(0.1, 10.0);
   fSlider_W->SetPosition(0.5, 2.5);
   fSlider_W->SetPointerPosition(2.0);
   fSlider_W->Connect("PointerPositionChanged()", "MyMainFrame", this, "DoSlider()");
   fSlider_W->Connect("PositionChanged()", "MyMainFrame", this, "DoSlider()");
   gf_kine->AddFrame(fSlider_W,new TGLayoutHints(kLHintsExpandX,4,4,2,5));

   TGHorizontalFrame * fHframe4 = new TGHorizontalFrame(gf_kine, fSideBarWidth, 30 );
   fTeh1_W = new TGTextEntry(fHframe4, fTbh1_W = new TGTextBuffer(5), TEId1_W);
   fTeh2_W = new TGTextEntry(fHframe4, fTbh2_W = new TGTextBuffer(5), TEId2_W);
   fTeh3_W = new TGTextEntry(fHframe4, fTbh3_W = new TGTextBuffer(5), TEId3_W);
   fTeh1_W->SetWidth(50);
   fTeh2_W->SetWidth(50);
   fTeh3_W->SetWidth(50);
   fTeh1_W->SetToolTipText("Minimum (left) Value of Slider");
   fTeh2_W->SetToolTipText("Pointer Position Value");
   fTeh3_W->SetToolTipText("Maximum (right) Value of Slider");
   fTbh1_W->AddText(0, "0.0");
   fTbh2_W->AddText(0, "0.0");
   fTbh3_W->AddText(0, "0.0");

   fTeh1_W->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fTeh2_W->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fTeh3_W->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fHframe4->AddFrame(fTeh1_W, fBfly2);
   fHframe4->AddFrame(fTeh2_W, fBfly1);
   fHframe4->AddFrame(fTeh3_W, fBfly3);
   fHframe4->Resize(fSideBarWidth, 25);
   gf_kine->AddFrame(fHframe4,new TGLayoutHints(kLHintsExpandX,0,0,0,0));

   //-----------
   // nu slider
   TGLabel * nu_label = new TGLabel(gf_kine,"nu (GeV):");
   gf_kine->AddFrame(nu_label,new TGLayoutHints(kLHintsLeft,0,0,0,0));

   fSlider_nu = new TGTripleHSlider(gf_kine,100,kDoubleScaleBoth,HSId_nu, kHorizontalFrame);
   fSlider_nu->SetConstrained(kTRUE);
   fSlider_nu->SetRange(0.0001, 10.0);
   fSlider_nu->SetPosition(0.5, 2.5);
   fSlider_nu->SetPointerPosition(40);
   fSlider_nu->Connect("PointerPositionChanged()", "MyMainFrame", this, "DoSlider()");
   fSlider_nu->Connect("PositionChanged()", "MyMainFrame", this, "DoSlider()");
   gf_kine->AddFrame(fSlider_nu,new TGLayoutHints(kLHintsExpandX,4,4,2,5));

   TGHorizontalFrame * fHframe5 = new TGHorizontalFrame(gf_kine, fSideBarWidth, 30 );
   fTeh1_nu = new TGTextEntry(fHframe5, fTbh1_nu = new TGTextBuffer(5), TEId1_nu);
   fTeh2_nu = new TGTextEntry(fHframe5, fTbh2_nu = new TGTextBuffer(5), TEId2_nu);
   fTeh3_nu = new TGTextEntry(fHframe5, fTbh3_nu = new TGTextBuffer(5), TEId3_nu);
   fTeh1_nu->SetWidth(50);
   fTeh2_nu->SetWidth(50);
   fTeh3_nu->SetWidth(50);
   fTeh1_nu->SetToolTipText("Minimum (left) Value of Slider");
   fTeh2_nu->SetToolTipText("Pointer Position Value");
   fTeh3_nu->SetToolTipText("Maximum (right) Value of Slider");
   fTbh1_nu->AddText(0, "0.0");
   fTbh2_nu->AddText(0, "0.0");
   fTbh3_nu->AddText(0, "0.0");

   fTeh1_nu->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fTeh2_nu->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fTeh3_nu->Connect("TextChanged(char*)", "MyMainFrame", this, "DoText(char*)");
   fHframe5->AddFrame(fTeh1_nu, fBfly2);
   fHframe5->AddFrame(fTeh2_nu, fBfly1);
   fHframe5->AddFrame(fTeh3_nu, fBfly3);
   fHframe5->Resize(fSideBarWidth, 25);
   gf_kine->AddFrame(fHframe5,new TGLayoutHints(kLHintsExpandX,0,0,0,0));

   fVertLeftFrame->AddFrame(gf_kine, new TGLayoutHints(kLHintsLeft | kLHintsExpandX,0,0,0,0));
   // end kinematics

   // -------------------------------
   // Buttons
   TGHorizontalFrame * fHframe10 = new TGHorizontalFrame(fVertLeftFrame, fSideBarWidth, 20 );

   TGTextButton *fTextButton1375 = new TGTextButton(fHframe10,"Draw");
   fTextButton1375->SetTextJustify(36);
   fTextButton1375->SetMargins(0,0,0,0);
   fTextButton1375->SetWrapLength(-1);
   fTextButton1375->Resize(37,22);
   fTextButton1375->Connect("Clicked()","MyMainFrame",this,"DoDraw()");
   fHframe10->AddFrame(fTextButton1375, new TGLayoutHints(kLHintsLeft,0,0,0,0));

   TGTextButton *fTextButton1376 = new TGTextButton(fHframe10,"Clear");
   fTextButton1376->SetTextJustify(36);
   fTextButton1376->SetMargins(0,0,0,0);
   fTextButton1376->SetWrapLength(-1);
   fTextButton1376->Resize(38,22);
   fTextButton1376->Connect("Clicked()","MyMainFrame",this,"DoClear()");
   fHframe10->AddFrame(fTextButton1376, new TGLayoutHints(kLHintsLeft,0,0,0,0));

   fVertLeftFrame->AddFrame(fHframe10, new TGLayoutHints(kLHintsLeft | kLHintsCenterX,0,0,0,0));

   // -----------------
   // Progress bar
   fHProg = new TGHProgressBar(fVertLeftFrame,TGProgressBar::kFancy,fSideBarWidth);
   fHProg->SetBarColor("lightblue");
   fHProg->SetPosition(0);
   fHProg->SetHeight(10);

   //fHProg->ShowPosition(kTRUE,kFALSE,"%.0f events");
   fVertLeftFrame->AddFrame(fHProg, new TGLayoutHints(kLHintsCenterX | kLHintsBottom,0,0,0,0));

   fHorizontalFrame1->AddFrame(fVertLeftFrame, new TGLayoutHints(kLHintsLeft | kLHintsTop | kLHintsExpandY));
   // done with side bar.


   // ---------------------------------------------------------------------
   // Right frame with plots
   TGVerticalFrame *fVertFrameCanvas = new TGVerticalFrame(fHorizontalFrame1,0,0,0);
   fVertFrameCanvas->SetName("fVertFrameCanvas");

   // ---------------------------------
   // tab widget
   TGTab *fTab1378 = new TGTab(fVertFrameCanvas,fWidth-fSideBarWidth,fHeight-20);

   // container of "Tab1"
   fPlotTab1 = new RCPlotTab(fTab1378);
   fTab1378->AddTab("E'",fPlotTab1);
   fPlotTab1->fDrawButton->Connect("Clicked()","MyMainFrame",this,"DoDraw1()");

   // container of "Tab2"
   fPlotTab2 = new RCPlotTab(fTab1378);
   fTab1378->AddTab("theta",fPlotTab2);
   fPlotTab2->fDrawButton->Connect("Clicked()","MyMainFrame",this,"DoDraw2()");

   // container of "Tab3"
   fPlotTab3 = new RCPlotTab(fTab1378);
   fTab1378->AddTab("W",fPlotTab3);
   fPlotTab3->fDrawButton->Connect("Clicked()","MyMainFrame",this,"DoDraw3()");

   // container of "Tab4"
   fPlotTab4 = new RCPlotTab(fTab1378);
   fTab1378->AddTab("E loss",fPlotTab4);
   fPlotTab4->fDrawButton->Connect("Clicked()","MyMainFrame",this,"DoDraw4()");

   fTab1378->SetTab(0);
   fTab1378->Resize(fTab1378->GetDefaultSize());

   fVertFrameCanvas->AddFrame(fTab1378, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

   fHorizontalFrame1->AddFrame(fVertFrameCanvas, new TGLayoutHints(kLHintsLeft | kLHintsTop | kLHintsExpandX | kLHintsExpandY));

   fMainFrame->AddFrame(fHorizontalFrame1, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,1,1,1,1));

   // ------------------------
   // Status bar
   Int_t parts[] = {33, 10, 10, 47};
   fStatusBar = new TGStatusBar(fMainFrame,50,10,kHorizontalFrame);
   fStatusBar->SetParts(parts,4);
   fMainFrame->AddFrame(fStatusBar, new TGLayoutHints(kLHintsBottom | kLHintsLeft | kLHintsExpandX, 0, 0, 2, 0));

   // fill status bar fields with information; selected is the object
   // below the cursor; atext contains pixel coordinates info
   fStatusBar->SetText("ready",0);

   // ------------------------
   // Status bar
   fMainFrame->SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
   fMainFrame->MapSubwindows();

   fMainFrame->Resize(fMainFrame->GetDefaultSize());
   fMainFrame->MapWindow();
   fMainFrame->Resize(fWidth,fHeight);

   fWmin  = 0.8;
   fWmax  = 4.0;
   fNumin = 0.0001;
   fNumax = 2.0;
   fQ2    = 5.0;

   if(!fDiffXSec) fDiffXSec = new  InSANERadiator<F1F209eInclusiveDiffXSec>();

   DoSlider();
   SetBorn(false);
   SetExternalOnly(fIsExternalOnly);
   SetInternalOnly(fIsInternalOnly);
   DoSlider();
   SetXSec(1);
   SetXSec(0);
}  
//______________________________________________________________________________
MyMainFrame::~MyMainFrame() {
   // Clean up used widgets: frames, buttons, layout hints
   if(fMainFrame) {
      fMainFrame->Cleanup();
      delete fMainFrame;
   }
}
//______________________________________________________________________________
void MyMainFrame::Destroy(){
   fMainFrame = 0; // To prevent segfault after "Xing" the window.
}
//______________________________________________________________________________
void MyMainFrame::SetBorn(Bool_t val){
   if(val) {
      // Is Born -> Turn off options
      fCheckButton_equivrad->SetDisabledAndSelected(fCheckButton_equivrad->IsOn());
      fCheckButton_internal->SetDisabledAndSelected(fCheckButton_internal->IsOn());
      fCheckButton_external->SetDisabledAndSelected(fCheckButton_external->IsOn());
      fCheckButton_anglepeaking->SetDisabledAndSelected(fCheckButton_anglepeaking->IsOn());
      fTE_tr->SetEnabled(false);
   } else {
      fCheckButton_equivrad->SetEnabled();
      //jfCheckButton_internal->SetEnabled();
      //jfCheckButton_external->SetEnabled();
      fCheckButton_anglepeaking->SetEnabled();
      fTE_tr->SetEnabled(true);
      SetExternalOnly(fIsExternalOnly);
      SetInternalOnly(fIsInternalOnly);
   }
   SetXSec(fXSec_id);
}
//___________________________________________________________________
void MyMainFrame::SetEquivRad(Bool_t val){
   if(val) {
      fTE_tr->SetEnabled(true);
   } else {
      fTE_tr->SetEnabled(false);
   }
   SetXSec(fXSec_id);
}
//___________________________________________________________________
void MyMainFrame::SetAnglePeaking(Bool_t val){
   if(val) {

   }
}
//___________________________________________________________________
void MyMainFrame::SetInternalOnly(Bool_t val){
   fIsInternalOnly = val;
   if(val) {
      fCheckButton_external->SetDisabledAndSelected(fCheckButton_equivrad->IsOn());
      fNumberEntry_Tb->SetState(false);
      fNumberEntry_Ta->SetState(false);
   } else {
      fCheckButton_external->SetEnabled();
      fNumberEntry_Tb->SetState(true);
      fNumberEntry_Ta->SetState(true);
   }
   SetXSec(fXSec_id);
}
//______________________________________________________________________________
void MyMainFrame::SetExternalOnly(Bool_t val){
   fIsExternalOnly = val;
   if(val) {
      fCheckButton_equivrad->SetDisabledAndSelected(fCheckButton_equivrad->IsOn());
      fCheckButton_internal->SetDisabledAndSelected(fCheckButton_internal->IsOn());
      //fCheckButton_external->SetDisabledAndSelected(fCheckButton_external->IsOn());
      fCheckButton_anglepeaking->SetDisabledAndSelected(fCheckButton_anglepeaking->IsOn());
      fTE_tr->SetEnabled(false);

      fNumberEntry_Tb->SetState(true);
      fNumberEntry_Ta->SetState(true);
   } else {
      fCheckButton_equivrad->SetEnabled();
      fCheckButton_internal->SetEnabled();
      //fCheckButton_external->SetEnabled();
      fCheckButton_anglepeaking->SetEnabled();
      fTE_tr->SetEnabled(true);

   }
   SetXSec(fXSec_id);
}
//___________________________________________________________________
void MyMainFrame::SetTarget(Int_t i ) {
   std::cout << " SetTarget " << i << std::endl;
   if( i == 0 ){
      fTargetNucleus = InSANENucleus::Proton();
   } else if( i == 1 ) {
      fTargetNucleus = InSANENucleus::Neutron();
   } else if( i == 2 ) {
      fTargetNucleus = InSANENucleus::Deuteron();
   } else if( i == 3 ) {
      fTargetNucleus = InSANENucleus::He3();
   } else if( i == 4 ) {
      fTargetNucleus = InSANENucleus::He4();
   } else if( i == 5 ) {
      fTargetNucleus = InSANENucleus(6,6);
   } else if( i == 6 ) {
      fTargetNucleus = InSANENucleus::Fe56();
   }
   fNumberEntry_Z->SetNumber(fTargetNucleus.GetZ());
   fNumberEntry_A->SetNumber(fTargetNucleus.GetA());
}
//______________________________________________________________________________
void MyMainFrame::DoTr() {

   InSANERadiator<F1F209eInclusiveDiffXSec> * xs = dynamic_cast<InSANERadiator<F1F209eInclusiveDiffXSec>*>(fDiffXSec);
   double tr = 0;
   if(xs) {
      tr = xs->GetRADCOR()->GetTr(fQ2);
   } else {
      tr = 0.0;
   }
   char buf[32];
   sprintf(buf, "%.6f", tr);
   fTB_tr->Clear();
   fTB_tr->AddText(0, buf);
   fTE_tr->SetCursorPosition(fTE_tr->GetCursorPosition());
   fTE_tr->Deselect();
   gClient->NeedRedraw(fTE_tr);

}
//______________________________________________________________________________
void MyMainFrame::DoSlider() {

   char buf[32];

   UpdateKine();
   DoTr();

   fSlider_eprime->SetRange(0.0001,fBeamEnergy);
   fSlider_eprime->SetPosition(fEmin,fEmax);
   //fSlider_eprime->SetCursorPosition(fSlider_eprime->GetCursorPosition());
   fSlider_eprime->DrawPointer();
   //gClient->NeedRedraw(fSlider_eprime);


   fSlider_theta->SetPosition(fTheta_min,fTheta_max);

   fSlider_W->SetPointerPosition(fW);
   fSlider_W->SetPosition(fWmin,fWmax);

   fSlider_nu->SetPointerPosition(fNu);
   fSlider_nu->SetPosition(fNumin,fNumax);

   // Update Q2
   sprintf(buf, "%.3f", fQ2);
   fTB_Q2->Clear();
   fTB_Q2->AddText(0,buf);
   fTE_Q2->SetCursorPosition(fTE_Q2->GetCursorPosition());
   fTE_Q2->Deselect();
   gClient->NeedRedraw(fTE_Q2);

   // ------------------------------------------
   // Update sliders 

   // Theta slider
   sprintf(buf, "%.3f", fSlider_theta->GetMinPosition());
   fTbh1_theta->Clear();
   fTbh1_theta->AddText(0, buf);
   fTeh1_theta->SetCursorPosition(fTeh1_theta->GetCursorPosition());
   fTeh1_theta->Deselect();
   gClient->NeedRedraw(fTeh1_theta);

   sprintf(buf, "%.3f", fSlider_theta->GetPointerPosition());
   fTbh2_theta->Clear();
   fTbh2_theta->AddText(0, buf);
   fTeh2_theta->SetCursorPosition(fTeh2_theta->GetCursorPosition());
   fTeh2_theta->Deselect();
   gClient->NeedRedraw(fTeh2_theta);

   sprintf(buf, "%.3f", fSlider_theta->GetMaxPosition());
   fTbh3_theta->Clear();
   fTbh3_theta->AddText(0, buf);
   fTeh3_theta->SetCursorPosition(fTeh3_theta->GetCursorPosition());
   fTeh3_theta->Deselect();
   gClient->NeedRedraw(fTeh3_theta);

   // Eprime
   sprintf(buf, "%.3f", fSlider_eprime->GetMinPosition());
   fTbh1_eprime->Clear();
   fTbh1_eprime->AddText(0, buf);
   fTeh1_eprime->SetCursorPosition(fTeh1_eprime->GetCursorPosition());
   fTeh1_eprime->Deselect();
   gClient->NeedRedraw(fTeh1_eprime);

   sprintf(buf, "%.3f", fSlider_eprime->GetPointerPosition());
   fTbh2_eprime->Clear();
   fTbh2_eprime->AddText(0, buf);
   fTeh2_eprime->SetCursorPosition(fTeh2_eprime->GetCursorPosition());
   fTeh2_eprime->Deselect();
   gClient->NeedRedraw(fTeh2_eprime);

   sprintf(buf, "%.3f", fSlider_eprime->GetMaxPosition());
   fTbh3_eprime->Clear();
   fTbh3_eprime->AddText(0, buf);
   fTeh3_eprime->SetCursorPosition(fTeh3_eprime->GetCursorPosition());
   fTeh3_eprime->Deselect();
   gClient->NeedRedraw(fTeh3_eprime);

   // W slider
   sprintf(buf, "%.3f", fSlider_W->GetMinPosition());
   fTbh1_W->Clear();
   fTbh1_W->AddText(0, buf);
   fTeh1_W->SetCursorPosition(fTeh1_W->GetCursorPosition());
   fTeh1_W->Deselect();
   gClient->NeedRedraw(fTeh1_W);

   sprintf(buf, "%.3f", fSlider_W->GetPointerPosition());
   fTbh2_W->Clear();
   fTbh2_W->AddText(0, buf);
   fTeh2_W->SetCursorPosition(fTeh2_W->GetCursorPosition());
   fTeh2_W->Deselect();
   gClient->NeedRedraw(fTeh2_W);

   sprintf(buf, "%.3f", fSlider_W->GetMaxPosition());
   fTbh3_W->Clear();
   fTbh3_W->AddText(0, buf);
   fTeh3_W->SetCursorPosition(fTeh3_W->GetCursorPosition());
   fTeh3_W->Deselect();
   gClient->NeedRedraw(fTeh3_W);

   // nu
   sprintf(buf, "%.3f", fSlider_nu->GetMinPosition());
   fTbh1_nu->Clear();
   fTbh1_nu->AddText(0, buf);
   fTeh1_nu->SetCursorPosition(fTeh1_nu->GetCursorPosition());
   fTeh1_nu->Deselect();
   gClient->NeedRedraw(fTeh1_nu);

   sprintf(buf, "%.3f", fSlider_nu->GetPointerPosition());
   fTbh2_nu->Clear();
   fTbh2_nu->AddText(0, buf);
   fTeh2_nu->SetCursorPosition(fTeh2_nu->GetCursorPosition());
   fTeh2_nu->Deselect();
   gClient->NeedRedraw(fTeh2_nu);

   sprintf(buf, "%.3f", fSlider_nu->GetMaxPosition());
   fTbh3_nu->Clear();
   fTbh3_nu->AddText(0, buf);
   fTeh3_nu->SetCursorPosition(fTeh3_nu->GetCursorPosition());
   fTeh3_nu->Deselect();
   gClient->NeedRedraw(fTeh3_nu);

   //fFitFcn->SetParameters(fSlider_theta->GetPointerPosition(), 0, 1);
   //fFitFcn->SetRange(fSlider_theta->GetMinPosition()-0.05,
   //      fSlider_theta->GetMaxPosition());
   //fFitFcn->Draw();
   //fCanvas->GetCanvas()->Modified();
   //fCanvas->GetCanvas()->Update();
}
//___________________________________________________________________
void MyMainFrame::DoClear() {
   fPlotTab1->Clear();
   fPlotTab2->Clear();
   fPlotTab3->Clear();
   fPlotTab4->Clear();
}
//______________________________________________________________________________
void MyMainFrame::DrawCanvas1(){

   TCanvas * c1  = fPlotTab1->fCanvas1;
   TMultiGraph * mg = fPlotTab1->fMG1;
   TLegend * leg = fPlotTab1->fLegend;

   c1->cd();
   c1->Update();

   int npar     = 2;
   TF1 * sigma1 = new TF1("sigma1", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         fEmin, fEmax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma1->SetParameter(0,fTheta);
   sigma1->SetParameter(1,fPhi);
   sigma1->SetNpx(fNPoints);
   sigma1->SetLineColor(fColor);
   sigma1->SetLineStyle(fStyle);

   TGraph * gr = 0;
   gr          = new TGraph(sigma1->DrawCopy("same"));
   mg->Add(gr,"l");
   mg->Draw("a");
   mg->GetXaxis()->SetTitle("E' (GeV)");
   mg->GetXaxis()->CenterTitle();
   //fMG1.GetXaxis()->SetTitle("E' (GeV)");

   leg->AddEntry(gr,fDiffXSec->GetPlotTitle(),"l");
   leg->Draw();

   c1->Modified(true);

   c1->Update();
   //delete sigma1;
}
//______________________________________________________________________________
void MyMainFrame::DrawCanvas2(){

   TCanvas * c1     = fPlotTab2->fCanvas1;
   TMultiGraph * mg = fPlotTab2->fMG1;
   TLegend * leg = fPlotTab2->fLegend;

   c1->cd();
   c1->Update();

   //fEprime    = fNumberEntry_eprime->GetNumber();
   fEprime    = fSlider_eprime->GetPointerPosition();


   int npar     = 2;
   TF1 * sigma2 = new TF1("sigma2", fDiffXSec, &InSANEInclusiveDiffXSec::PolarAngleDependentXSec_deg, 
         fTheta_min,fTheta_max, npar,"InSANEInclusiveDiffXSec","PolarAngleDependentXSec_deg");
   sigma2->SetParameter(0,fEprime);
   sigma2->SetParameter(1,fPhi);
   sigma2->SetNpx(fNPoints);
   sigma2->SetLineColor(fColor);
   sigma2->SetLineStyle(fStyle);

   TGraph * gr = 0;
   gr          = new TGraph(sigma2->DrawCopy("same"));
   mg->Add(gr,"l");
   mg->Draw("a");
   mg->GetXaxis()->SetTitle("#theta (radians)");
   mg->GetXaxis()->CenterTitle();

   leg->AddEntry(gr,fDiffXSec->GetPlotTitle(),"l");
   leg->Draw();

   c1->Modified(true);
   c1->Update();
   //delete sigma2;

}
//______________________________________________________________________________
void MyMainFrame::DrawCanvas3(){

   TCanvas * c1     = fPlotTab3->fCanvas1;
   TMultiGraph * mg = fPlotTab3->fMG1;
   TLegend * leg = fPlotTab3->fLegend;

   c1->cd();
   c1->Update();

   int npar     = 2;
   TF1 * sigma3 = new TF1("sigma3", fDiffXSec, &InSANEInclusiveDiffXSec::WDependentXSec, 
         fWmin, fWmax, npar,"InSANEInclusiveDiffXSec","WDependentXSec");
   sigma3->SetParameter(0,fQ2);
   sigma3->SetParameter(1,fPhi);
   sigma3->SetNpx(fNPoints);
   sigma3->SetLineColor(fColor);
   sigma3->SetLineStyle(fStyle);

   TGraph * gr = 0;
   gr          = new TGraph(sigma3->DrawCopy("same"));
   mg->Add(gr,"l");
   mg->Draw("a");
   mg->GetXaxis()->SetTitle("W (GeV)");
   mg->GetXaxis()->CenterTitle();

   leg->AddEntry(gr,fDiffXSec->GetPlotTitle(),"l");
   leg->Draw();

   c1->Modified(true);
   c1->Update();
   //delete sigma1;

}
//______________________________________________________________________________
void MyMainFrame::DrawCanvas4(){

   TCanvas     * c1  = fPlotTab4->fCanvas1;
   TMultiGraph * mg  = fPlotTab4->fMG1;
   TLegend     * leg = fPlotTab4->fLegend;

   c1->cd();
   c1->Update();

   int npar     = 2;
   TF1 * sigma4 = new TF1("sigma4", fDiffXSec, &InSANEInclusiveDiffXSec::PhotonEnergyDependentXSec, 
         fNumin, fNumax, npar,"InSANEInclusiveDiffXSec","PhotonEnergyDependentXSec");
   sigma4->SetParameter(0,fTheta);
   sigma4->SetParameter(1,fPhi);
   sigma4->SetNpx(fNPoints);
   sigma4->SetLineColor(fColor);
   sigma4->SetLineStyle(fStyle);

   TGraph * gr = 0;
   gr          = new TGraph(sigma4->DrawCopy("same"));
   mg->Add(gr,"l");
   mg->Draw("a");
   mg->GetXaxis()->SetTitle("#omega (GeV)");
   mg->GetXaxis()->CenterTitle();

   leg->AddEntry(gr,fDiffXSec->GetPlotTitle(),"l");
   leg->Draw();

   c1->Modified(true);
   c1->Update();
   //delete sigma1;
}
//______________________________________________________________________________
void MyMainFrame::UpdateKine() {

   fColor      = fNumberEntry_color->GetNumber();
   fStyle      = fNumberEntry_style->GetNumber();
   fNPoints    = fNumberEntry_npoints->GetNumber();

   fBeamEnergy = fNumberEntry_ebeam->GetNumber();
   fTheta      = fSlider_theta->GetPointerPosition()*degree;
   fPhi        = 0.0;
   fEprime     = fSlider_eprime->GetPointerPosition();

   fQ2         = InSANE::Kine::Qsquared(fBeamEnergy,fEprime,fTheta);
   fx          = InSANE::Kine::xBjorken_EEprimeTheta(fBeamEnergy,fEprime,fTheta);
   fNu         = fBeamEnergy - fEprime;
   fW          = InSANE::Kine::W_EEprimeTheta(fBeamEnergy,fEprime,fTheta);
   

   fEmin       = fSlider_eprime->GetMinPosition();
   fEmax       = fSlider_eprime->GetMaxPosition();
   if(fEmax > fBeamEnergy) fEmax = fBeamEnergy;

   fTheta_min  = fSlider_theta->GetMinPosition();
   fTheta_max  = fSlider_theta->GetMaxPosition();

   fWmin       = fSlider_W->GetMinPosition();
   fWmax       = fSlider_W->GetMaxPosition();
   fNumin       = fSlider_nu->GetMinPosition();
   fNumax       = fSlider_nu->GetMaxPosition();
   if(fWmin > fW )   fWmin = fW;
   if(fWmax < fW )   fWmax = fW;
   if(fNumin > fNu ) fNumin = fNu;
   if(fNumax < fNu ) fNumax = fNu;

}
//______________________________________________________________________________
void MyMainFrame::SetXSecHelicity(Int_t h) {
   if( h == BGId_HelZero ) {
      fHelicity = 0;
   } else if( h == BGId_HelPlus ) {
      fHelicity = 1;
   } else if( h == BGId_HelMinus ) {
      fHelicity = -1;
   }
}
//______________________________________________________________________________
void MyMainFrame::SetXSec(Int_t xsec_id) {
   // Controls the various options.
   // Sets them according to what various cross sections allow
   // or make any sense.

   if( xsec_id  == 0 ) {
      // F1F209 DIS
      if( xsec_id != fXSec_id) {
         fCheckButton_equivrad->SetEnabled();
         fCheckButton_internal->SetEnabled();
         fR_hel[0]->SetDisabledAndSelected(false);
         fR_hel[1]->SetState(kButtonDown);
         fR_hel[2]->SetDisabledAndSelected(false);
         fTE_tr->SetEnabled(true);
      }
   } else if( xsec_id == 1 ) {
      // F1F209 Quasi Elastic
      if( xsec_id != fXSec_id) {
         fCheckButton_equivrad->SetEnabled();
         fCheckButton_internal->SetEnabled();
         fR_hel[0]->SetDisabledAndSelected(false);
         fR_hel[1]->SetState(kButtonDown);
         fR_hel[2]->SetDisabledAndSelected(false);
         fTE_tr->SetEnabled(true);
      }
   } else if( xsec_id == 2 ) {
      // POLRAD DIS 
      if( xsec_id != fXSec_id) {
         fCheckButton_internal->SetDisabledAndSelected(true);
         fCheckButton_equivrad->SetDisabledAndSelected(fCheckButton_equivrad->IsOn());
         fR_hel[0]->SetEnabled(true);
         fR_hel[2]->SetEnabled(true);
      }
   } else if( xsec_id == 3 ) {
      // POLRAD Quasi Elastic 
      if( xsec_id != fXSec_id) {
         fCheckButton_internal->SetDisabledAndSelected(true);
         fCheckButton_equivrad->SetDisabledAndSelected(fCheckButton_equivrad->IsOn());
         fR_hel[0]->SetEnabled(true);
         fR_hel[2]->SetEnabled(true);
      }
   } else if( xsec_id == 4 ) {
      // POLRAD Elastic 
      if( xsec_id != fXSec_id) {
         fCheckButton_internal->SetDisabledAndSelected(true);
         fCheckButton_equivrad->SetDisabledAndSelected(fCheckButton_equivrad->IsOn());
         fR_hel[0]->SetEnabled(true);
         fR_hel[2]->SetEnabled(true);
      }

   }

   fXSec_id = xsec_id;
}
//______________________________________________________________________________
void MyMainFrame::UpdateXSec() {
   // Sets and configures the cross section according to the settings

   int xsec_id = 0;
   TGLBEntry * entry = fListBox_xsec->GetSelectedEntry();
   if(entry) xsec_id = entry->EntryId();

   //std::cout << " xsec_id " << xsec_id  << std::endl;

   if( xsec_id  == 0 ) {
      // F1F209 DIS

      if( fCheckButton_born->IsOn() ) {
         if(fDiffXSec) delete fDiffXSec;
         //std::cout << " born xsec_id " << xsec_id << " delete"  << std::endl;
         fDiffXSec = new  F1F209eInclusiveDiffXSec();

      } else {
         InSANERadiator<F1F209eInclusiveDiffXSec> * xs = dynamic_cast<InSANERadiator<F1F209eInclusiveDiffXSec>*>(fDiffXSec);
         if(xs) {
            std::cout << " using existing" << std::endl;
            fDiffXSec = xs;
         } else {
            delete fDiffXSec;
            //std::cout << " xsec_id " << xsec_id << " delete"  << std::endl;
            xs = new  InSANERadiator<F1F209eInclusiveDiffXSec>();
            fDiffXSec = xs;
         }
         if(fCheckButton_internal->IsOn() ) {
            xs->SetInternalOnly(true);
         }else{
            xs->SetInternalOnly(false);
         }

      }
   } else if( xsec_id  == 1 ) {
      // F1F209 QuasiElastic

      if( fCheckButton_born->IsOn() ) {

         if(fDiffXSec) delete fDiffXSec;
         //std::cout << " born xsec_id " << xsec_id << " delete"  << std::endl;
         fDiffXSec = new  F1F209QuasiElasticDiffXSec();

      } else {
         InSANERadiator<F1F209QuasiElasticDiffXSec> * xs = dynamic_cast<InSANERadiator<F1F209QuasiElasticDiffXSec>*>(fDiffXSec);
         if(xs) {
            std::cout << " using existing" << std::endl;
            fDiffXSec = xs;
         } else {
            delete fDiffXSec;
            std::cout << " xsec_id " << xsec_id << " delete"  << std::endl;
            fDiffXSec = new  InSANERadiator<F1F209QuasiElasticDiffXSec>();
         }
      }

   } else if( xsec_id  == 2 ) {
      // POLRAD DIS

      if( fCheckButton_born->IsOn() ) {
         if(fDiffXSec) delete fDiffXSec;
         InSANEPOLRADBornDiffXSec * xs = new  InSANEPOLRADBornDiffXSec();
         fDiffXSec = xs;
         xs->GetPOLRAD()->SetHelicity(0);
      } else {
         InSANEPOLRADInelasticTailDiffXSec * xs = dynamic_cast<InSANEPOLRADInelasticTailDiffXSec*>(fDiffXSec);
         if(xs) {
            fDiffXSec = xs;
         } else {
            std::cout << " xsec_id " << xsec_id << " delete"  << std::endl;
            delete fDiffXSec;
            fDiffXSec =  new  InSANEPOLRADInelasticTailDiffXSec();
            InSANEPOLRADInelasticTailDiffXSec * xs2 = dynamic_cast<InSANEPOLRADInelasticTailDiffXSec*>(fDiffXSec);
            xs2->GetPOLRAD()->SetHelicity(0);
         }
      }

   } else if( xsec_id  == 3 ) {
      // POLRAD QUasiElastic

      InSANEPOLRADQuasiElasticTailDiffXSec * xs = dynamic_cast<InSANEPOLRADQuasiElasticTailDiffXSec*>(fDiffXSec);
      if(xs) {
         fDiffXSec = xs;
      } else {
         std::cout << " xsec_id " << xsec_id << " delete"  << std::endl;
         delete fDiffXSec;
         fDiffXSec =  new  InSANEPOLRADQuasiElasticTailDiffXSec();
         InSANEPOLRADQuasiElasticTailDiffXSec * xs2 = dynamic_cast<InSANEPOLRADQuasiElasticTailDiffXSec*>(fDiffXSec);
         xs2->GetPOLRAD()->SetHelicity(0);
      }

   } else if( xsec_id  == 4 ) {
      // POLRAD Elastic

      InSANEPOLRADElasticTailDiffXSec * xs = dynamic_cast<InSANEPOLRADElasticTailDiffXSec*>(fDiffXSec);
      if(xs) {
         fDiffXSec = xs;
      } else {
         std::cout << " xsec_id " << xsec_id << " delete"  << std::endl;
         delete fDiffXSec;
         fDiffXSec =  new  InSANEPOLRADElasticTailDiffXSec();
         InSANEPOLRADElasticTailDiffXSec * xs2 = dynamic_cast<InSANEPOLRADElasticTailDiffXSec*>(fDiffXSec);
         xs2->GetPOLRAD()->SetHelicity(0);
      }

   } else {
      // OTHER
      std::cout << xsec_id << std::endl;
      InSANERadiator<InSANEInclusiveDiffXSec> * xs = dynamic_cast<InSANERadiator<InSANEInclusiveDiffXSec>*>(fDiffXSec);
      if(xs) {
         fDiffXSec = xs;
      } else {
         delete fDiffXSec;
         fDiffXSec = new  InSANERadiator<InSANEInclusiveDiffXSec>();
      }
   }
   InSANERadiator<F1F209eInclusiveDiffXSec> * radxs = dynamic_cast<InSANERadiator<F1F209eInclusiveDiffXSec>*>(fDiffXSec);
   if(radxs){
      double r1 = fNumberEntry_Tb->GetNumber();
      double r2 = fNumberEntry_Ta->GetNumber();
      std::cout << "setting radiation lengths: " << r1 << ", " << r2 << std::endl;
      radxs->SetRadiationLength(r1,r2);
   }
   fDiffXSec->SetBeamEnergy(fBeamEnergy);
   fDiffXSec->SetHelicity(fHelicity);
   fDiffXSec->SetTargetNucleus(fTargetNucleus);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   fDiffXSec->UsePhaseSpace(false);

}
//______________________________________________________________________________
void MyMainFrame::DoDraw1() {
   
   fStatusBar->SetText("calculating...");
   fStatusBar->SetText("calculating",0);
   fMainFrame->Layout();

   fHProg->SetPosition(0);


   fHProg->SetPosition(10);
   UpdateXSec();

   fHProg->SetPosition(25);
   DrawCanvas1();

   fHProg->SetPosition(50);
   //DrawCanvas2();

   fHProg->SetPosition(65);
   //DrawCanvas3();

   fHProg->SetPosition(85);
   //DrawCanvas4();

   fHProg->SetPosition(100);
   fHProg->Reset();
   fStatusBar->SetText("done.",0);
   fMainFrame->Layout();

}
//______________________________________________________________________________
void MyMainFrame::DoDraw() {
   
   fStatusBar->SetText("calculating...");
   fStatusBar->SetText("calculating",0);
   fMainFrame->Layout();

   fHProg->SetPosition(0);


   fHProg->SetPosition(10);
   UpdateXSec();

   fHProg->SetPosition(25);
   DrawCanvas1();

   fHProg->SetPosition(50);
   DrawCanvas2();

   fHProg->SetPosition(65);
   DrawCanvas3();

   fHProg->SetPosition(85);
   DrawCanvas4();

   fHProg->SetPosition(100);
   fHProg->Reset();
   fStatusBar->SetText("done.",0);
   fMainFrame->Layout();

}
//______________________________________________________________________________
void MyMainFrame::DoDraw2() {
   fStatusBar->SetText("calculating...");
   fStatusBar->SetText("calculating",0);
   fMainFrame->Layout();
   fHProg->SetPosition(0);
   fHProg->SetPosition(10);
   UpdateXSec();
   fHProg->SetPosition(25);
   //DrawCanvas1();
   fHProg->SetPosition(50);
   DrawCanvas2();
   fHProg->SetPosition(65);
   //DrawCanvas3();
   fHProg->SetPosition(85);
   //DrawCanvas4();
   fHProg->SetPosition(100);
   fHProg->Reset();
   fStatusBar->SetText("done.",0);
   fMainFrame->Layout();
}
//______________________________________________________________________________
void MyMainFrame::DoDraw3() {
   fStatusBar->SetText("calculating...");
   fStatusBar->SetText("calculating",0);
   fMainFrame->Layout();
   fHProg->SetPosition(0);
   fHProg->SetPosition(10);
   UpdateXSec();
   fHProg->SetPosition(25);
   //DrawCanvas1();
   fHProg->SetPosition(50);
   //DrawCanvas2();
   fHProg->SetPosition(65);
   DrawCanvas3();
   fHProg->SetPosition(85);
   //DrawCanvas4();
   fHProg->SetPosition(100);
   fHProg->Reset();
   fStatusBar->SetText("done.",0);
   fMainFrame->Layout();
}
//______________________________________________________________________________
void MyMainFrame::DoDraw4() {
   fStatusBar->SetText("calculating...");
   fStatusBar->SetText("calculating",0);
   fMainFrame->Layout();
   fHProg->SetPosition(0);
   fHProg->SetPosition(10);
   UpdateXSec();
   fHProg->SetPosition(25);
   //DrawCanvas1();
   fHProg->SetPosition(50);
   //DrawCanvas2();
   fHProg->SetPosition(65);
   //DrawCanvas3();
   fHProg->SetPosition(85);
   DrawCanvas4();
   fHProg->SetPosition(100);
   fHProg->Reset();
   fStatusBar->SetText("done.",0);
   fMainFrame->Layout();
}
//______________________________________________________________________________

