#include "RCPlotTab.h"


//______________________________________________________________________________
RCPlotTab::RCPlotTab(const TGWindow* p, UInt_t w, UInt_t h, UInt_t options, Pixel_t back):
      TGCompositeFrame(p, w, h, options, back) {

   fLogX1 = false;
   fLogY1 = false;
   fMG1   = new TMultiGraph();
   fLegend = new TLegend(0.12,0.6,0.5,0.88);

   this->SetLayoutManager(new TGVerticalLayout(this));

   // Plot Buttons
   fHFrame_buttons = new TGHorizontalFrame(this,68,22,kHorizontalFrame);
   // x1
   fCheckButton_LogX1 = new TGCheckButton(fHFrame_buttons,"log x");
   fCheckButton_LogX1->SetTextJustify(36);
   fCheckButton_LogX1->SetMargins(0,0,0,0);
   fCheckButton_LogX1->SetWrapLength(-1);
   fCheckButton_LogX1->Connect("Toggled(Bool_t)","RCPlotTab",this,"LogX1(Bool_t)");
   fHFrame_buttons->AddFrame(fCheckButton_LogX1, new TGLayoutHints(kLHintsNormal));
   // y1
   fCheckButton_LogY1 = new TGCheckButton(fHFrame_buttons,"log y");
   fCheckButton_LogY1->SetTextJustify(36);
   fCheckButton_LogY1->SetMargins(0,0,0,0);
   fCheckButton_LogY1->SetWrapLength(-1);
   fCheckButton_LogY1->Connect("Toggled(Bool_t)","RCPlotTab",this,"LogY1(bool)");
   fHFrame_buttons->AddFrame(fCheckButton_LogY1, new TGLayoutHints(kLHintsNormal));

   this->AddFrame(fHFrame_buttons, new TGLayoutHints(kLHintsCenterX | kLHintsTop | kLHintsExpandX ,1,1,1,1));

   // embedded canvas
   fEmbeddedCanvas1 = new TRootEmbeddedCanvas(0,this,352,256);
   Int_t wfRootEmbeddedCanvas1 = fEmbeddedCanvas1->GetCanvasWindowId();
   fCanvas1 = new TCanvas("c123", 10, 10, wfRootEmbeddedCanvas1);
   fEmbeddedCanvas1->AdoptCanvas(fCanvas1);
   this->AddFrame(fEmbeddedCanvas1, new TGLayoutHints(kLHintsLeft | kLHintsTop | kLHintsExpandY | kLHintsExpandX ,2,2,2,2));

   // Plot button
   fDrawButton = new TGTextButton(fHFrame_buttons,"Draw");
   fDrawButton->SetTextJustify(36);
   fDrawButton->SetMargins(0,0,0,0);
   fDrawButton->SetWrapLength(-1);
   fDrawButton->Resize(37,22);
   fHFrame_buttons->AddFrame(fDrawButton, new TGLayoutHints(kLHintsNormal));

   fClearButton = new TGTextButton(fHFrame_buttons,"Clear");
   fClearButton->SetTextJustify(36);
   fClearButton->SetMargins(0,0,0,0);
   fClearButton->SetWrapLength(-1);
   fClearButton->Resize(38,22);
   fClearButton->Connect("Clicked()","RCPlotTab",this,"Clear()");
   fHFrame_buttons->AddFrame(fClearButton, new TGLayoutHints(kLHintsNormal));

}
//______________________________________________________________________________
RCPlotTab::~RCPlotTab(){

}
//______________________________________________________________________________
void RCPlotTab::LogX1(bool val){
   fCanvas1->cd();
   std::cout << "Setting LogX1" << std::endl;
   TAxis * xAxis = fMG1->GetXaxis();
   if(xAxis) {
      fX1_min = xAxis->GetXmin();
      fX1_max = xAxis->GetXmax();
      if( fX1_min <= 0.0 && val ) {
         fOldMinX1 = fX1_min;
         fX1_min   = 0.001;
         Warning("LogX1","Changing the axis minimum to 0.001");
         xAxis->SetLimits(fX1_min,fX1_max);
      } else {
         fX1_min = fOldMinX1;
         Warning("LogX1","Changing axis minimum to previous value.");
         xAxis->SetLimits(fX1_min,fX1_max);
      }
   }
   fCanvas1->SetLogx(val);
   fCanvas1->Update();
}
//______________________________________________________________________________
void RCPlotTab::LogY1(bool val){
   fCanvas1->cd();
   std::cout << "Setting LogY1" << std::endl;
   TAxis * xAxis = fMG1->GetYaxis();
   if(xAxis) {
      fY1_min = xAxis->GetXmin();
      fY1_max = xAxis->GetXmax();
      if( fX1_min <= 0.0 && val ) {
         fOldMinY1 = fY1_min;
         fY1_min   = 0.001;
         Warning("LogY1","Changing the axis minimum to 0.001");
         xAxis->SetRangeUser(fY1_min,fY1_max);
      } else {
         fY1_min = fOldMinY1;
         Warning("LogY1","Changing axis minimum to previous value.");
         xAxis->SetRangeUser(fY1_min,fY1_max);
      }
   }
   fCanvas1->SetLogy(val);
   fCanvas1->Update();
}
//______________________________________________________________________________
void RCPlotTab::Clear(){
   fLegend->Clear();
   delete fMG1;
   fMG1 = new TMultiGraph();
   fCanvas1->Clear();
   fCanvas1->Update();
}

