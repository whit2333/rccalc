#ifndef RCPlotTab_HH
#define RCPlotTab_HH 1

#include "TGProgressBar.h"
#include "TGStatusBar.h"
#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
#include <RQ_OBJECT.h>

#include "TMultiGraph.h"
#include "TAxis.h"
#include "TLegend.h"

class RCPlotTab : public TGCompositeFrame {

   public:

      RCPlotTab(const TGWindow* p = 0, UInt_t w = 1, UInt_t h = 1, UInt_t options = 0, Pixel_t back = GetDefaultFrameBackground());
      virtual ~RCPlotTab();

      TCanvas              * fCanvas1         ;
      TRootEmbeddedCanvas  * fEmbeddedCanvas1 ;
      bool                   fLogX1;
      bool                   fLogY1;
      double                 fX1_min;
      double                 fX1_max;
      double                 fY1_min;
      double                 fY1_max;
      double                 fOldMinX1;
      double                 fOldMinY1;
      TGCheckButton        * fCheckButton_LogX1;
      TGCheckButton        * fCheckButton_LogY1;
      TGHorizontalFrame    * fHFrame_buttons;

      TGTextButton *fDrawButton;
      TGTextButton *fClearButton;

      TMultiGraph  * fMG1;
      TLegend      * fLegend;

      void LogX1(bool);
      void LogY1(bool);

      void Clear();

      ClassDef(RCPlotTab,1)
};

#endif

