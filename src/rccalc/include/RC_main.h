#ifndef RC_main_HH
#define RC_main_HH 1

#include "RCPlotTab.h"

#include "TGProgressBar.h"
#include "TGStatusBar.h"
#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
#include <RQ_OBJECT.h>
#include "TGNumberEntry.h"

#include "TGFrame.h"
#include "TGFileDialog.h"
#include "TGCanvas.h"
#include "TGButton.h"
#include "TGFSComboBox.h"
#include "TGLabel.h"
#include "TGTab.h"
#include "TGListView.h"
#include "TGSplitter.h"
#include "TGTextEntry.h"
#include "TGStatusBar.h"
#include "TGToolBar.h"
#include "TRootEmbeddedCanvas.h"
#include "TCanvas.h"
#include "TGObject.h"

#include "TGButtonGroup.h"

#include "Riostream.h"

#include "InSANEPOLRADInternalPolarizedDiffXSec.h"
#include "InSANEepElasticDiffXSec.h"
#include "F1F209eInclusiveDiffXSec.h"
#include "InSANERadiator.h"
#include "InSANENucleus.h"

#include "TPaveText.h"
#include "TMultiGraph.h"

#include "TGTripleSlider.h"

enum ETestCommandIdentifiers {
   HId1,
   HId2,
   HId3,
   HCId1,
   HCId2,
   HSId1,

   HSId_eprime,
   TEId1_eprime,
   TEId2_eprime,
   TEId3_eprime,

   HSId_theta,
   TEId1_theta,
   TEId2_theta,
   TEId3_theta,

   HSId_W,
   TEId1_W,
   TEId2_W,
   TEId3_W,

   HSId_nu,
   TEId1_nu,
   TEId2_nu,
   TEId3_nu,

   TEId_Q2,
   TEId_tr,

   BGId_HelPlus,
   BGId_HelMinus,
   BGId_HelZero
};

class MyMainFrame {

   RQ_OBJECT("MyMainFrame")

   private:
      int   fWidth;
      int   fSideBarWidth;
      int   fHeight;

      TGMainFrame         * fMainFrame;    // Main frame
      TGHorizontalFrame   * fHorizontalFrame1;
      TGVerticalFrame     * fVertLeftFrame;

   protected:

      TGNumberEntry *fNumberEntry_npoints;
      TGNumberEntry *fNumberEntry_color;
      TGNumberEntry *fNumberEntry_style;
      TGNumberEntry *fNumberEntry_Tb;
      TGNumberEntry *fNumberEntry_Ta;
      TGComboBox    *fListBox_targets;
      TGNumberEntry *fNumberEntry_Z;
      TGNumberEntry *fNumberEntry_A;
      TGComboBox    *fListBox_xsec;
      TGNumberEntry *fNumberEntry_xmin;
      TGNumberEntry *fNumberEntry_xmax;
      TGNumberEntry *fNumberEntry_ebeam;
      TGNumberEntry *fNumberEntry_eprime;
      TGNumberEntry *fNumberEntry_phi;

      //TGTripleHSlider     * fSlider_theta;
      //TGTextEntry         * fTeh1, *fTeh2, *fTeh3;
      //TGTextBuffer        * fTbh1, *fTbh2, *fTbh3;

      TGTripleHSlider     * fSlider_theta;
      TGTextEntry         * fTeh1_theta, *fTeh2_theta, *fTeh3_theta;
      TGTextBuffer        * fTbh1_theta, *fTbh2_theta, *fTbh3_theta;

      TGTripleHSlider     * fSlider_eprime;
      TGTextEntry         * fTeh1_eprime, *fTeh2_eprime, *fTeh3_eprime;
      TGTextBuffer        * fTbh1_eprime, *fTbh2_eprime, *fTbh3_eprime;

      TGTripleHSlider     * fSlider_W;
      TGTextEntry         * fTeh1_W, *fTeh2_W, *fTeh3_W;
      TGTextBuffer        * fTbh1_W, *fTbh2_W, *fTbh3_W;

      TGTripleHSlider     * fSlider_nu;
      TGTextEntry         * fTeh1_nu, *fTeh2_nu, *fTeh3_nu;
      TGTextBuffer        * fTbh1_nu, *fTbh2_nu, *fTbh3_nu;

      TGLayoutHints       *fBly, *fBfly1, *fBfly2, *fBfly3;

      TGLabel          *fLProg;      // progress label
      TGHProgressBar   *fHProg;      // progress bar
      TGStatusBar      *fStatusBar;  // status bar

      TGTextEntry         * fTE_Q2;
      TGTextBuffer        * fTB_Q2;

      TGTextEntry         * fTE_tr;
      TGTextBuffer        * fTB_tr;

      TGCheckButton *fCheckButton_born;
      TGCheckButton *fCheckButton_equivrad;
      TGCheckButton *fCheckButton_internal;
      TGCheckButton *fCheckButton_external;
      TGCheckButton *fCheckButton_anglepeaking;

      TGButtonGroup * fRadioButtons_helicity;
      TGRadioButton * fR_hel[3];


      // Limits
      double fEmin;
      double fEmax;
      double fTheta_min;
      double fTheta_max;
      double fWmin;
      double fWmax;
      double fNumin;
      double fNumax;

      // kinematics
      double                fHelicity;
      double                fBeamEnergy;
      double                fEprime;
      double                fTheta;
      double                fPhi;
      double                fNu;
      double                fW;
      double                fQ2;
      double                fx;
      InSANENucleus         fTargetNucleus;

      int                   fNPoints;
      int                   fColor;
      int                   fStyle;

      int                   fXSec_id;
      bool                  fIsInternalOnly;
      bool                  fIsExternalOnly;

      void DrawCanvas1();  // vs Energy
      void DrawCanvas2();  // vs Angle
      void DrawCanvas3();  // vs W
      void DrawCanvas4();  // vs W

      RCPlotTab            * fPlotTab1;
      RCPlotTab            * fPlotTab2;
      RCPlotTab            * fPlotTab3;
      RCPlotTab            * fPlotTab4;

      InSANEInclusiveDiffXSec * fDiffXSec ;

   public:

      MyMainFrame(const TGWindow *p,UInt_t w = 900,UInt_t h = 800);
      virtual ~MyMainFrame();

      void Destroy();

      void DoSlider();
      void DoTr();

      void DoDraw();         // Draw all canvases 
      void DoDraw1();        // Draw canvas 1 only 
      void DoDraw2();        // Draw canvas 2 only 
      void DoDraw3();        // Draw canvas 3 only 
      void DoDraw4();        // Draw canvas 4 only 

      void DoClear();       // clear button
      void UpdateXSec();
      void SetXSec(Int_t);
      void UpdateKine();

      void SetXSecHelicity(Int_t);

      void LogX1(bool);
      void LogY1(bool);

      void SetTarget(Int_t i);
      void SetBorn(Bool_t val);
      void SetEquivRad(Bool_t val);
      void SetInternalOnly(Bool_t val);
      void SetExternalOnly(Bool_t val);
      void SetAnglePeaking(Bool_t val);


      ClassDef(MyMainFrame, 2)
};

#endif

