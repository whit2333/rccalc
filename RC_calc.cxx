#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
#include <RQ_OBJECT.h>

// By ROOT version 5.34/18 on 2014-08-04 11:50:18

#ifndef ROOT_TGDockableFrame
#include "TGDockableFrame.h"
#endif
#ifndef ROOT_TGMenu
#include "TGMenu.h"
#endif
#ifndef ROOT_TGMdiDecorFrame
#include "TGMdiDecorFrame.h"
#endif
#ifndef ROOT_TG3DLine
#include "TG3DLine.h"
#endif
#ifndef ROOT_TGMdiFrame
#include "TGMdiFrame.h"
#endif
#ifndef ROOT_TGMdiMainFrame
#include "TGMdiMainFrame.h"
#endif
#ifndef ROOT_TGMdiMenu
#include "TGMdiMenu.h"
#endif
#ifndef ROOT_TGListBox
#include "TGListBox.h"
#endif
#ifndef ROOT_TGNumberEntry
#include "TGNumberEntry.h"
#endif
#ifndef ROOT_TGScrollBar
#include "TGScrollBar.h"
#endif
#ifndef ROOT_TGComboBox
#include "TGComboBox.h"
#endif
#ifndef ROOT_TGuiBldHintsEditor
#include "TGuiBldHintsEditor.h"
#endif
#ifndef ROOT_TRootBrowser
#include "TRootBrowser.h"
#endif
#ifndef ROOT_TGuiBldNameFrame
#include "TGuiBldNameFrame.h"
#endif
#ifndef ROOT_TGFrame
#include "TGFrame.h"
#endif
#ifndef ROOT_TGFileDialog
#include "TGFileDialog.h"
#endif
#ifndef ROOT_TGShutter
#include "TGShutter.h"
#endif
#ifndef ROOT_TGButtonGroup
#include "TGButtonGroup.h"
#endif
#ifndef ROOT_TGCommandPlugin
#include "TGCommandPlugin.h"
#endif
#ifndef ROOT_TGCanvas
#include "TGCanvas.h"
#endif
#ifndef ROOT_TGFSContainer
#include "TGFSContainer.h"
#endif
#ifndef ROOT_TGuiBldEditor
#include "TGuiBldEditor.h"
#endif
#ifndef ROOT_TGColorSelect
#include "TGColorSelect.h"
#endif
#ifndef ROOT_TGTextEdit
#include "TGTextEdit.h"
#endif
#ifndef ROOT_TGButton
#include "TGButton.h"
#endif
#ifndef ROOT_TGFSComboBox
#include "TGFSComboBox.h"
#endif
#ifndef ROOT_TGLabel
#include "TGLabel.h"
#endif
#ifndef ROOT_TGView
#include "TGView.h"
#endif
#ifndef ROOT_TGMsgBox
#include "TGMsgBox.h"
#endif
#ifndef ROOT_TRootGuiBuilder
#include "TRootGuiBuilder.h"
#endif
#ifndef ROOT_TGFileBrowser
#include "TGFileBrowser.h"
#endif
#ifndef ROOT_TGTab
#include "TGTab.h"
#endif
#ifndef ROOT_TGListView
#include "TGListView.h"
#endif
#ifndef ROOT_TGSplitter
#include "TGSplitter.h"
#endif
#ifndef ROOT_TGTextEntry
#include "TGTextEntry.h"
#endif
#ifndef ROOT_TGStatusBar
#include "TGStatusBar.h"
#endif
#ifndef ROOT_TGToolBar
#include "TGToolBar.h"
#endif
#ifndef ROOT_TRootEmbeddedCanvas
#include "TRootEmbeddedCanvas.h"
#endif
#ifndef ROOT_TCanvas
#include "TCanvas.h"
#endif
#ifndef ROOT_TGuiBldDragManager
#include "TGuiBldDragManager.h"
#endif
#ifndef ROOT_TGObject
#include "TGObject.h"
#endif

#include "Riostream.h"
#include "InSANERadiator.h"
#include "F1F209eInclusiveDiffXSec.h"
#include "InSANENucleus.h"
#include "TPaveText.h"

//-----------------------------------
//InSANERadiator<F1F209eInclusiveDiffXSec> * fDiffXSec4 = new  InSANERadiator<F1F209eInclusiveDiffXSec>();
//fDiffXSec4->SetBeamEnergy(fBeamEnergy);
//fDiffXSec4->SetTargetNucleus(Target);
//fDiffXSec4->InitializePhaseSpaceVariables();
//fDiffXSec4->InitializeFinalStateParticles();
//______________________________________________________________________________
class MyMainFrame {

   RQ_OBJECT("MyMainFrame")

   private:
      TGMainFrame         *fMain;
      TRootEmbeddedCanvas *fEcanvas;

   public:
      MyMainFrame(const TGWindow *p,UInt_t w,UInt_t h);
      virtual ~MyMainFrame();
      void DoDraw();
      void drawTest();

      double                fBeamEnergy;
      InSANENucleus   fTargetNucleus;

      TCanvas              * fCanvas1               ;
      TRootEmbeddedCanvas  * fEmbeddedCanvas1;

      TCanvas              * fCanvas2               ;
      TRootEmbeddedCanvas  * fEmbeddedCanvas2;

      TGNumberEntry *fNumberEntry_xmin;// = new TGNumberEntry(fVertLeftFrame, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
      TGNumberEntry *fNumberEntry_xmax;// = new TGNumberEntry(fVertLeftFrame, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);

      InSANEInclusiveDiffXSec * fDiffXSec ;

      ClassDef(MyMainFrame, 2)
};
//______________________________________________________________________________
MyMainFrame::MyMainFrame(const TGWindow *p,UInt_t w,UInt_t h) {

   fTargetNucleus = InSANENucleus::Proton();
   fCanvas1                = 0;
   fEmbeddedCanvas1    = 0;
   fCanvas2                = 0;
   fEmbeddedCanvas2    = 0;
   fBeamEnergy = 4.7;

   fDiffXSec = 0;


   // main frame
   TGMainFrame *fMainFrame1686 = new TGMainFrame(gClient->GetRoot(),10,10,kMainFrame | kVerticalFrame);
   fMainFrame1686->SetName("fMainFrame1686");
   fMainFrame1686->SetLayoutBroken(kTRUE);

   // composite frame
   TGCompositeFrame *fMainFrame2666 = new TGCompositeFrame(fMainFrame1686,659,425,kVerticalFrame);
   fMainFrame2666->SetName("fMainFrame2666");
   fMainFrame2666->SetLayoutBroken(kTRUE);

   // composite frame
   TGCompositeFrame *fMainFrame3218 = new TGCompositeFrame(fMainFrame2666,863,473,kVerticalFrame);
   fMainFrame3218->SetName("fMainFrame3218");

   // horizontal frame
   TGHorizontalFrame *fHorizontalFrame1908 = new TGHorizontalFrame(fMainFrame3218,861,471,kHorizontalFrame);
   fHorizontalFrame1908->SetName("fHorizontalFrame1908");

   // vertical frame
   TGVerticalFrame *fVertLeftFrame = new TGVerticalFrame(fHorizontalFrame1908,118,471,kVerticalFrame);
   fVertLeftFrame->SetName("fVertLeftFrame");

   TGFont *ufont;         // will reflect user font changes
   ufont = gClient->GetFont("-*-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1");

   TGGC   *uGC;           // will reflect user GC changes
   // graphics context changes
   GCValues_t valEntry1353;
   valEntry1353.fMask = kGCForeground | kGCBackground | kGCFillStyle | kGCFont | kGCGraphicsExposures;
   gClient->GetColorByName("#000000",valEntry1353.fForeground);
   gClient->GetColorByName("#e0e0e0",valEntry1353.fBackground);
   valEntry1353.fFillStyle = kFillSolid;
   valEntry1353.fFont = ufont->GetFontHandle();
   valEntry1353.fGraphicsExposures = kFALSE;
   uGC = gClient->GetGC(&valEntry1353, kTRUE);
   TGTextEntry *fTextEntry1353 = new TGTextEntry(fVertLeftFrame, new TGTextBuffer(31),-1,uGC->GetGC(),ufont->GetFontStruct(),kSunkenFrame | kDoubleBorder | kOwnBackground);
   fTextEntry1353->SetMaxLength(300);
   fTextEntry1353->SetAlignment(kTextLeft);
   fTextEntry1353->SetText("RCcalc_test");
   fTextEntry1353->Resize(93,fTextEntry1353->GetDefaultHeight());
   fTextEntry1353->SetToolTipText("Output Name (no extention)");
   fVertLeftFrame->AddFrame(fTextEntry1353, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));

   fNumberEntry_xmin = new TGNumberEntry(fVertLeftFrame, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
   fNumberEntry_xmin->SetName("xmin");
   fNumberEntry_xmin->SetLimits(TGNumberFormat::kNELLimitMinMax,0.0,1000.0);
   fNumberEntry_xmin->SetNumber(0.5);
   fVertLeftFrame->AddFrame(fNumberEntry_xmin, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));

   fNumberEntry_xmax = new TGNumberEntry(fVertLeftFrame, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
   fNumberEntry_xmax->SetName("xmax");
   fNumberEntry_xmax->SetLimits(TGNumberFormat::kNELLimitMinMax,0.0,1000.0);
   fNumberEntry_xmax->SetNumber(2.5);
   fVertLeftFrame->AddFrame(fNumberEntry_xmax, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));

   TGNumberEntry *fNumberEntry1974 = new TGNumberEntry(fVertLeftFrame, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
   fNumberEntry1974->SetName("fNumberEntry1974");
   fVertLeftFrame->AddFrame(fNumberEntry1974, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));

   TGLabel *fLabel1364 = new TGLabel(fVertLeftFrame,"fLabel2460");
   fLabel1364->SetTextJustify(36);
   fLabel1364->SetMargins(0,0,0,0);
   fLabel1364->SetWrapLength(-1);
   fVertLeftFrame->AddFrame(fLabel1364, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));

   // horizontal frame
   TGHorizontalFrame *fHorizontalFrame2469 = new TGHorizontalFrame(fVertLeftFrame,68,22,kHorizontalFrame);
   fHorizontalFrame2469->SetName("fHorizontalFrame2469");
   TGLabel *fLabel1366 = new TGLabel(fHorizontalFrame2469,"Q2");
   fLabel1366->SetTextJustify(36);
   fLabel1366->SetMargins(0,0,0,0);
   fLabel1366->SetWrapLength(-1);
   fHorizontalFrame2469->AddFrame(fLabel1366, new TGLayoutHints(kLHintsLeft | kLHintsCenterX | kLHintsTop | kLHintsCenterY,2,2,2,2));
   TGLabel *fLabel1367 = new TGLabel(fHorizontalFrame2469,"GeV");
   fLabel1367->SetTextJustify(36);
   fLabel1367->SetMargins(0,0,0,0);
   fLabel1367->SetWrapLength(-1);
   fHorizontalFrame2469->AddFrame(fLabel1367, new TGLayoutHints(kLHintsRight | kLHintsTop | kLHintsCenterY));
   TGNumberEntry *fNumberEntry1951 = new TGNumberEntry(fHorizontalFrame2469, (Double_t) 1,3,-1,(TGNumberFormat::EStyle) 5,(TGNumberFormat::EAttribute) 1,(TGNumberFormat::ELimit) 1,0.0001,1e+06);
   fNumberEntry1951->SetName("fNumberEntry1951");
   fHorizontalFrame2469->AddFrame(fNumberEntry1951, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

   fVertLeftFrame->AddFrame(fHorizontalFrame2469, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));

   // vertical frame
   TGVerticalFrame *fVerticalFrame2480 = new TGVerticalFrame(fVertLeftFrame,2,2,kVerticalFrame);
   fVerticalFrame2480->SetName("fVerticalFrame2480");
   fVerticalFrame2480->SetLayoutBroken(kTRUE);

   fVertLeftFrame->AddFrame(fVerticalFrame2480, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   TGCheckButton *fCheckButton1373 = new TGCheckButton(fVertLeftFrame,"fTextButton2002");
   fCheckButton1373->SetTextJustify(36);
   fCheckButton1373->SetMargins(0,0,0,0);
   fCheckButton1373->SetWrapLength(-1);
   fVertLeftFrame->AddFrame(fCheckButton1373, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   TGCheckButton *fCheckButton1374 = new TGCheckButton(fVertLeftFrame,"fTextButton2020");
   fCheckButton1374->SetTextJustify(36);
   fCheckButton1374->SetMargins(0,0,0,0);
   fCheckButton1374->SetWrapLength(-1);
   fVertLeftFrame->AddFrame(fCheckButton1374, new TGLayoutHints(kLHintsNormal));

   TGTextButton *fTextButton1375 = new TGTextButton(fVertLeftFrame,"Draw");
   fTextButton1375->SetTextJustify(36);
   fTextButton1375->SetMargins(0,0,0,0);
   fTextButton1375->SetWrapLength(-1);
   fTextButton1375->Resize(37,22);
   fTextButton1375->Connect("Clicked()","MyMainFrame",this,"drawTest()");

   fVertLeftFrame->AddFrame(fTextButton1375, new TGLayoutHints(kLHintsNormal));
   TGTextButton *fTextButton1376 = new TGTextButton(fVertLeftFrame,"Clear");
   fTextButton1376->SetTextJustify(36);
   fTextButton1376->SetMargins(0,0,0,0);
   fTextButton1376->SetWrapLength(-1);
   fTextButton1376->Resize(38,22);
   fVertLeftFrame->AddFrame(fTextButton1376, new TGLayoutHints(kLHintsNormal));

   fHorizontalFrame1908->AddFrame(fVertLeftFrame, new TGLayoutHints(kLHintsLeft | kLHintsTop | kLHintsExpandY));

   // vertical frame
   TGVerticalFrame *fVertFrameCanvas = new TGVerticalFrame(fHorizontalFrame1908,743,471,kVerticalFrame);
   fVertFrameCanvas->SetName("fVertFrameCanvas");

   // tab widget
   TGTab *fTab1378 = new TGTab(fVertFrameCanvas,384,288);

   // container of "Tab1"
   TGCompositeFrame *fCompositeFrame1381;
   fCompositeFrame1381 = fTab1378->AddTab("Tab1");
   fCompositeFrame1381->SetLayoutManager(new TGVerticalLayout(fCompositeFrame1381));

   // embedded canvas
   fEmbeddedCanvas1 = new TRootEmbeddedCanvas(0,fCompositeFrame1381,352,256);
   Int_t wfRootEmbeddedCanvas1 = fEmbeddedCanvas1->GetCanvasWindowId();
   TCanvas *c123 = new TCanvas("c123", 10, 10, wfRootEmbeddedCanvas1);
   fEmbeddedCanvas1->AdoptCanvas(c123);
   fCompositeFrame1381->AddFrame(fEmbeddedCanvas1, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   fCanvas1 = fEmbeddedCanvas1->GetCanvas();


   // container of "Tab2"
   TGCompositeFrame *fCompositeFrame1396;
   fCompositeFrame1396 = fTab1378->AddTab("Tab2");
   fCompositeFrame1396->SetLayoutManager(new TGVerticalLayout(fCompositeFrame1396));

   // embedded canvas
   fEmbeddedCanvas2 = new TRootEmbeddedCanvas(0,fCompositeFrame1396,304,208);
   Int_t wfRootEmbeddedCanvas2 = fEmbeddedCanvas2->GetCanvasWindowId();
   TCanvas *c124 = new TCanvas("c124", 10, 10, wfRootEmbeddedCanvas2);
   fEmbeddedCanvas2->AdoptCanvas(c124);
   fCompositeFrame1396->AddFrame(fEmbeddedCanvas2, new TGLayoutHints(kLHintsLeft | kLHintsTop,2,2,2,2));
   fCanvas2 = fEmbeddedCanvas2->GetCanvas();


   fTab1378->SetTab(1);

   fTab1378->Resize(fTab1378->GetDefaultSize());
   fVertFrameCanvas->AddFrame(fTab1378, new TGLayoutHints(kLHintsNormal));

   fHorizontalFrame1908->AddFrame(fVertFrameCanvas, new TGLayoutHints(kLHintsLeft | kLHintsTop | kLHintsExpandX | kLHintsExpandY));

   fMainFrame3218->AddFrame(fHorizontalFrame1908, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,1,1,1,1));

   fMainFrame2666->AddFrame(fMainFrame3218, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
   fMainFrame3218->MoveResize(0,0,863,473);

   fMainFrame1686->AddFrame(fMainFrame2666, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
   fMainFrame2666->MoveResize(0,0,659,425);

   fMainFrame1686->SetMWMHints(kMWMDecorAll,
                        kMWMFuncAll,
                        kMWMInputModeless);
   fMainFrame1686->MapSubwindows();

   fMainFrame1686->Resize(fMainFrame1686->GetDefaultSize());
   fMainFrame1686->MapWindow();
   fMainFrame1686->Resize(677,443);
}  
//______________________________________________________________________________
MyMainFrame::~MyMainFrame() {
   // Clean up used widgets: frames, buttons, layout hints
   fMain->Cleanup();
   delete fMain;
}
//______________________________________________________________________________
void MyMainFrame::DoDraw() {
   // Draws function graphics in randomly chosen interval

   if(!fDiffXSec) fDiffXSec = new InSANERadiator<F1F209eInclusiveDiffXSec>();

   std::cout << "test 0" << std::endl;
   //fDiffXSec = new F1F209eInclusiveDiffXSec();
   fDiffXSec->SetBeamEnergy(fBeamEnergy);
   fDiffXSec->SetTargetNucleus(fTargetNucleus);
   fDiffXSec->InitializePhaseSpaceVariables();
   fDiffXSec->InitializeFinalStateParticles();
   //fDiffXSec->Refresh();
   std::cout << "test 1" << std::endl;
   
   double Emin = fNumberEntry_xmin->GetNumber();
   double Emax = fNumberEntry_xmax->GetNumber();
   double theta = 30.0*degree;
   double phi = 0.0;
   int npar = 2;


   //TF1 * sigma4 = new TF1("sigma4","x*[0]+x*[1]",Emin,Emax); 
   TF1 * sigma4 = new TF1("sigma", fDiffXSec, &InSANEInclusiveDiffXSec::EnergyDependentXSec, 
         Emin, Emax, npar,"InSANEInclusiveDiffXSec","EnergyDependentXSec");
   sigma4->SetParameter(0,theta);
   sigma4->SetParameter(1,phi);
   sigma4->SetNpx(10);
   sigma4->SetLineColor(kBlue);
   //leg->AddEntry(sigma4,"template","l");
   //fDiffXSec4->Print();

   fCanvas2 = fEmbeddedCanvas2->GetCanvas();
   fCanvas2->cd();
   sigma4->Draw();
   fCanvas2->Update();

   //TF1 *f1 = new TF1("f1","sin(x)/x",0,gRandom->Rndm()*10);
   //f1->SetLineWidth(3);
   //f1->Draw();
   //TCanvas *fCanvas = fEcanvas->GetCanvas();
   //fCanvas->cd();
}


//______________________________________________________________________________
void MyMainFrame::drawTest(){
   DoDraw();
   const Int_t nx = 20;
   char *people[nx] = {"Jean","Pierre","Marie","Odile","Sebastien",
      "Fons","Rene","Nicolas","Xavier","Greg","Bjarne","Anton","Otto",
      "Eddy","Peter","Pasha","Philippe","Suzanne","Jeff","Valery"};
   //TCanvas *c1 = new TCanvas("c1","demo bin labels",10,10,900,500);
   //c1->SetGrid();
   //c1->SetTopMargin(0.15);
   fCanvas1 = fEmbeddedCanvas1->GetCanvas();
   fCanvas1->cd();
   
   TH1F *h = new TH1F("h","test",3,0,3);
   h->SetStats(0);
   h->SetFillColor(38);
   h->SetBit(TH1::kCanRebin);
   for (Int_t i=0;i<5000;i++) {
      Int_t r = gRandom->Rndm()*20;
      h->Fill(people[r],1);
   }
   h->LabelsDeflate();
   h->Draw();
   TPaveText *pt = new TPaveText(0.7,0.85,0.98,0.98,"brNDC");
   pt->SetFillColor(18);
   pt->SetTextAlign(12);
   pt->AddText("Use the axis Context Menu LabelsOption");
   pt->AddText(" \"a\"   to sort by alphabetic order");
   pt->AddText(" \">\"   to sort by decreasing values");
   pt->AddText(" \"<\"   to sort by increasing values");
   pt->Draw();

   fCanvas1->Update();
}
//______________________________________________________________________________

void RC_calc() {
      // Popup the GUI...
      new MyMainFrame(gClient->GetRoot(),400,400);
}
